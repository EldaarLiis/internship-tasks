//
//  ViewController.swift
//  lesson16
//
//  Created by Harbros 66 on 1.06.21.
//

import UIKit

class ViewController: UIViewController {
    private let drawingView = DrawingTask()
    private let myDrawingView = MyDrawing()
    private let paintView = Paint()

    override func viewDidLoad() {
        super.viewDidLoad()
//        addMyDrawingView()
//        addStarView()
        addPaintView()
    }

    private func addStarView() {
        view.addSubview(drawingView)
        drawingView.backgroundColor = .white
    }

    private func addMyDrawingView() {
        view.addSubview(myDrawingView)
        myDrawingView.backgroundColor = .white
    }

    private func addPaintView() {
        view.addSubview(paintView)
        paintView.backgroundColor = .white
    }

    override var prefersStatusBarHidden: Bool {
        return true
    }

    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
    }

    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        drawingView.frame = view.safeAreaLayoutGuide.layoutFrame
        drawingView.setNeedsDisplay()
        myDrawingView.frame = view.safeAreaLayoutGuide.layoutFrame
        myDrawingView.setNeedsDisplay()
        paintView.frame = view.safeAreaLayoutGuide.layoutFrame
        paintView.setNeedsDisplay()
    }
}
