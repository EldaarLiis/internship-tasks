//
//  Paint.swift
//  lesson16
//
//  Created by Harbros 66 on 5.06.21.
//

import UIKit

private enum DrawingConstants {
    static let lineWidth: CGFloat = 3
    static let strokeColor = UIColor.black.cgColor
}

class Paint: UIView {
    private typealias Const = DrawingConstants

    private var currentLocation: CGPoint = .zero
    private var lastLocation: CGPoint = .zero
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        if let touchLocation = touches.first?.location(in: self) {
            lastLocation = touchLocation
        }
    }

    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        if let touchLocation = touches.first?.location(in: self) {
            currentLocation = touchLocation
            drawLine(from: lastLocation, to: currentLocation)
            lastLocation = currentLocation
        }
    }

    private func drawLine(from: CGPoint, to: CGPoint) {
        let linePath = UIBezierPath()
        linePath.move(to: from)
        linePath.addLine(to: to)
        let lineLayer = CAShapeLayer()
        lineLayer.path = linePath.cgPath
        lineLayer.lineWidth = Const.lineWidth
        lineLayer.strokeColor = Const.strokeColor
        layer.addSublayer(lineLayer)
    }
}
