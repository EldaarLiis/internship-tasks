//
//  MyDrawing.swift
//  lesson16
//
//  Created by Harbros 66 on 4.06.21.
//

import UIKit

private enum MyDrawingConstants {
    static let halfSizeDivisionRate: CGFloat = 2

    static let inscribedCircleRate: CGFloat = 2 / 3
    static let radiationArcsNumber = 3
    static let radiationArcStep: CGFloat = 2

    static let boundaryCircleRadiusDivisionRate: CGFloat = 15

    // angles stuff
    static let firstLineAngle: CGFloat = 210 * radianToDegreeTranslation
    static let secondLineAngle: CGFloat = 330 * radianToDegreeTranslation
    static let thirdLineAngle: CGFloat = 90 * radianToDegreeTranslation
    static let thirtyDegreeInRad: CGFloat = 30 * radianToDegreeTranslation
    static let radianToDegreeTranslation: CGFloat = .pi / 180
    static let startingAngleForInnerCircle: CGFloat = 0
    static let endingAngleForInnerCircle: CGFloat = 2 * .pi
    static let radiationSignStartingAngle = -60 * radianToDegreeTranslation
    static let radiationSignStepAngle = 60 * radianToDegreeTranslation

    // circles radius stuff
    static let innerCircleRadiusDivisionRate: CGFloat = 3.5
    static let radiationOutsideCircleRadiusDivisionRate: CGFloat = 4
    static let radiationInnersideCircleRadiusDivisionRate: CGFloat = 9
    static let centerCircleRadiusDivisionRate: CGFloat = 15

    // style
    static let mainStrokeColor = UIColor.black.cgColor
    static let mainFillColor = UIColor.yellow.cgColor
    static let mainLineWidth: CGFloat = 7
    static let subLineWidth: CGFloat = 6
    static let innerCircleFillColor = UIColor.clear.cgColor
    static let radiationSignFillColor = UIColor.black.cgColor
}

class MyDrawing: UIView {
    private var screenSize: CGRect = .zero
    private var screenOffset: CGPoint = .zero
    private var minScreenSide: CGFloat = .zero
    private var innerCenter: CGFloat = .zero
    private typealias Const = MyDrawingConstants

    override func draw(_ rect: CGRect) {
        layer.sublayers = nil
        screenSize = rect
        getMiddleOffset()
        addLayers()
    }

    private func addLayers() {
        let boundaryLayer = CAShapeLayer()
        boundaryLayer.lineWidth = Const.mainLineWidth
        boundaryLayer.path = drawBoundaries().cgPath
        boundaryLayer.strokeColor = Const.mainStrokeColor
        boundaryLayer.fillColor = Const.mainFillColor
        layer.addSublayer(boundaryLayer)

        let innerCircleLayer = CAShapeLayer()
        innerCircleLayer.lineWidth = Const.subLineWidth
        innerCircleLayer.path = drawInnerCircle().cgPath
        innerCircleLayer.strokeColor = Const.mainStrokeColor
        innerCircleLayer.fillColor = Const.innerCircleFillColor
        layer.addSublayer(innerCircleLayer)

        (0 ..< Const.radiationArcsNumber).forEach { number in
            let wing = CAShapeLayer()
            wing.lineWidth = .zero
            wing.path = drawRadiationSign(number).cgPath
            wing.strokeColor = Const.mainStrokeColor
            wing.fillColor = Const.radiationSignFillColor
            layer.addSublayer(wing)
        }

        let centerPoint = CAShapeLayer()
        centerPoint.lineWidth = .zero
        centerPoint.path = drawCenterPoint().cgPath
        centerPoint.strokeColor = Const.mainStrokeColor
        centerPoint.fillColor = Const.radiationSignFillColor
        layer.addSublayer(centerPoint)
    }

    private func getMiddleOffset() {
        minScreenSide = min(screenSize.height, screenSize.width)
        screenOffset = CGPoint()
        if screenSize.height > screenSize.width {
            screenOffset.y = (screenSize.height - screenSize.width) / Const.halfSizeDivisionRate
        } else {
            screenOffset.x = (screenSize.width - screenSize.height) / Const.halfSizeDivisionRate
        }
    }

    private func drawBoundaries() -> UIBezierPath {
        let boundaryCirclesRadius = minScreenSide / Const.boundaryCircleRadiusDivisionRate
//        print(minScreenSide)

        let leftBottomCircleCenter = CGPoint(
            x: screenOffset.x + boundaryCirclesRadius,
            y: screenOffset.y + minScreenSide - boundaryCirclesRadius
        )
        let rightBottomCircleCenter = CGPoint(
            x: screenOffset.x + minScreenSide - boundaryCirclesRadius,
            y: screenOffset.y + minScreenSide - boundaryCirclesRadius
        )
        let topMiddleCircleCenter = CGPoint(
            x: screenOffset.x + minScreenSide / Const.halfSizeDivisionRate,
            y: screenOffset.y + boundaryCirclesRadius * Const.halfSizeDivisionRate
        )

        innerCenter = leftBottomCircleCenter.y - topMiddleCircleCenter.y
//        print(leftBottomCircleCenter)
//        print(rightBottomCircleCenter)
//        print(topMiddleCircleCenter)
        // will be divided into three lines for beautiful animation
        let boundaryPath = UIBezierPath()
        boundaryPath.addArc(
            withCenter: topMiddleCircleCenter,
            radius: boundaryCirclesRadius,
            startAngle: Const.firstLineAngle,
            endAngle: Const.secondLineAngle,
            clockwise: true
        )
        boundaryPath.addArc(
            withCenter: rightBottomCircleCenter,
            radius: boundaryCirclesRadius,
            startAngle: Const.secondLineAngle,
            endAngle: Const.thirdLineAngle,
            clockwise: true
        )
        boundaryPath.addArc(
            withCenter: leftBottomCircleCenter,
            radius: boundaryCirclesRadius,
            startAngle: Const.thirdLineAngle,
            endAngle: Const.firstLineAngle,
            clockwise: true
        )
        boundaryPath.addLine(
            to: getPointFromCircle(
                center: topMiddleCircleCenter,
                radius: boundaryCirclesRadius,
                angle: Const.firstLineAngle
            )
        )
        return boundaryPath
    }

    private func drawInnerCircle() -> UIBezierPath {
        let innerCirclePath = UIBezierPath()
        let innerCircleRadius = minScreenSide / Const.innerCircleRadiusDivisionRate
        let innerCircleCenter = CGPoint(
            x: screenOffset.x + minScreenSide / Const.halfSizeDivisionRate,
            y: screenOffset.y + minScreenSide * Const.inscribedCircleRate
        )

        innerCirclePath.addArc(
            withCenter: innerCircleCenter,
            radius: innerCircleRadius,
            startAngle: Const.startingAngleForInnerCircle,
            endAngle: Const.endingAngleForInnerCircle,
            clockwise: true
        )

        return innerCirclePath
    }

    private func drawRadiationSign(_ radiationArc: Int) -> UIBezierPath {
        let radiationSignPath = UIBezierPath()
        let radiationSignOutsideRadius = minScreenSide / Const.radiationOutsideCircleRadiusDivisionRate
        let radiationSignInnersideRadius = minScreenSide / Const.radiationInnersideCircleRadiusDivisionRate
        let radiationSignCenter = CGPoint(
            x: screenOffset.x + minScreenSide / Const.halfSizeDivisionRate,
            y: screenOffset.y + minScreenSide * Const.inscribedCircleRate
        )

        let startAngle = Const.radiationSignStartingAngle +
            CGFloat(radiationArc) * Const.radiationArcStep * Const.radiationSignStepAngle
        let endAngle = startAngle + Const.radiationSignStepAngle

        radiationSignPath.move(
            to: getPointFromCircle(
                center: radiationSignCenter,
                radius: radiationSignInnersideRadius,
                angle: startAngle
            )
        )
        radiationSignPath.addArc(
            withCenter: radiationSignCenter,
            radius: radiationSignOutsideRadius,
            startAngle: startAngle,
            endAngle: endAngle,
            clockwise: true
        )
        radiationSignPath.addArc(
            withCenter: radiationSignCenter,
            radius: radiationSignInnersideRadius,
            startAngle: endAngle,
            endAngle: startAngle,
            clockwise: false
        )

        return radiationSignPath
    }

    private func drawCenterPoint() -> UIBezierPath {
        let centerPoint = UIBezierPath()
        let centerPointRadius = minScreenSide / Const.centerCircleRadiusDivisionRate
        let radiationSignCenter = CGPoint(
            x: screenOffset.x + minScreenSide / Const.halfSizeDivisionRate,
            y: screenOffset.y + minScreenSide * Const.inscribedCircleRate
        )

        centerPoint.addArc(
            withCenter: radiationSignCenter,
            radius: centerPointRadius,
            startAngle: Const.startingAngleForInnerCircle,
            endAngle: Const.endingAngleForInnerCircle,
            clockwise: true
        )

        return centerPoint
    }

    private func getPointFromCircle(center: CGPoint, radius: CGFloat, angle: CGFloat) -> CGPoint {
        return CGPoint(
            x: center.x + radius * cos(angle),
            y: center.y + radius * sin(angle)
        )
    }
}
