//
//  DrawingTask.swift
//  lesson16
//
//  Created by Harbros 66 on 3.06.21.
//

import UIKit
extension UIColor {
    static func random() -> UIColor {
        return Self(
            red: CGFloat.random(in: 0.0...1.0),
            green: CGFloat.random(in: 0.0...1.0),
            blue: CGFloat.random(in: 0.0...1.0),
            alpha: 1
        )
    }
}

private enum DrawingConstants {
    // next value sets star origin boundaries (offset for x and y from where will star be drawn)
    static let zeroOriginForStarDrawing: CGFloat = .zero
    static let halfSizeDivisionRate: CGFloat = 2
    static let radianToDegreeTranslation = CGFloat.pi / 180
    static let zeroAngle: CGFloat = 0
    static let fullAngle: CGFloat = 2 * .pi

    static let circlesRadius: CGFloat = 10
    static let circlesStrokeColor: UIColor = .black
    static let circlesFillColor: UIColor = .clear

    // next value sets number of points of star
    // does work in range 3 ... infinity
    // but on values > 360 turns into circle and on values > 350 star is close to circle
    // looks magestic on big values :)
    static let starPoints = 5
    // next values are for calculations and should not be changed
    static let angleForStar: CGFloat = 360 / CGFloat(starPoints)
    static let pathStepOffsetInt = 1
    static let randomRollRange: Range<CGFloat> = (0..<angleForStar)
    // next value sets star "pointness" in 1... (starPoins/2), 2 - most pointy, 3 - less and so on
    // till value is > starPoints/2 or 1 - simple polygon for given points
    // doesn't work on values < 0 or >= starPoints
    // and actually should not be a multiple of starPoints because star will lose points
    // for example wont work if starPoints = 12 and this rate = 3
    static let pathStepDivisionRate = 2
    // next value sets entering point, should be in range 0 ..< starPoints
    static let pathEnteringPoint = 0
}

class DrawingTask: UIView {
    private var screenSize = CGRect.zero
    private var maxStarSize = CGFloat.zero
    private var wasDrawnOnce = false
    private let bigStarColor = UIColor.random()
    private var randomRoll: CGFloat = .zero

    private typealias Const = DrawingConstants

    override func draw(_ rect: CGRect) {
        screenSize = rect
        maxStarSize = min(rect.width, rect.height) / 4
        randomRoll = .random(in: Const.randomRollRange)

        let bigPath = bigStarPath(rect)
        bigStarColor.setFill()
        bigStarColor.setStroke()
        bigPath.fill()
        bigPath.stroke()

        let circlesPath = bigStarCircles(rect)
        Const.circlesStrokeColor.setStroke()
        Const.circlesFillColor.setFill()
        circlesPath.fill()
        circlesPath.stroke()

        // checks if its not a first draw
        if wasDrawnOnce {
            (1...5).forEach { _ in
                let path = starPath(getRandomOrigin())
                let randomColor = UIColor.random()
                randomColor.setFill()
                randomColor.setStroke()
                path.fill()
                path.stroke()
            }
        } else {
            wasDrawnOnce = true
        }
    }

    private func bigStarPath(_ screenSize: CGRect) -> UIBezierPath {
        let bigStarSize = min(screenSize.height, screenSize.width)

        return getPathsForPoints(getOffset(), bigStarSize)
    }

    private func bigStarCircles(_ screenSize: CGRect) -> UIBezierPath {
        let circlesPath = UIBezierPath()
        let bigStarSize = min(screenSize.height, screenSize.width)

        let points = getStarPointsCoordsFromOrigin(getOffset(), bigStarSize)

        points.forEach { point in
            circlesPath.move(
                to: CGPoint(
                    x: point.x + Const.circlesRadius,
                    y: point.y
                )
            )
            circlesPath.addArc(
                withCenter: point,
                radius: Const.circlesRadius,
                startAngle: Const.zeroAngle,
                endAngle: Const.fullAngle,
                clockwise: true
            )
        }
        guard let lastPoint = points.last else { return circlesPath }
        circlesPath.move(to: lastPoint)
        points.forEach { point in
            circlesPath.addLine(to: point)
        }

        return circlesPath
    }

    private func starPath(_ origin: CGPoint = .zero) -> UIBezierPath {
        return getPathsForPoints(origin, maxStarSize)
    }

    // MARK: - underhanded trickery
    // this method creates array of star points, and actually it works even for more points than 5
    private func getStarPointsCoordsFromOrigin(_ origin: CGPoint, _ size: CGFloat) -> [CGPoint] {
        // finds radius for star
        let radius = size / Const.halfSizeDivisionRate
        // finds origin for star
        let starCenter = CGPoint(
            x: origin.x + radius,
            y: origin.y + radius
        )
        var starPoints = [CGPoint]()

        // next cycle creates array of (x, y) coords for each point of star with math trickery
        (Int.zero ..< Const.starPoints).forEach { pointNumber in
            let point = CGPoint(
                x: starCenter.x + radius * cos(
                        Const.radianToDegreeTranslation * (randomRoll + CGFloat(pointNumber) * Const.angleForStar)
                    ),
                y: starCenter.y + radius * sin(
                    Const.radianToDegreeTranslation * (randomRoll + CGFloat(pointNumber) * Const.angleForStar)
                )
            )
            starPoints.append(point)
        }
        return starPoints
    }

    // this method created path for given points
    private func getPathsForPoints(_ origin: CGPoint, _ size: CGFloat) -> UIBezierPath {
        let path = UIBezierPath()
        let points = getStarPointsCoordsFromOrigin(origin, size)

        // check if points are not impty
        if points.isEmpty {
            return path
        }
        // sets step for star iterating
        let step = (points.count - Const.pathStepOffsetInt) / Const.pathStepDivisionRate
        // sets initial point from where will star path be generated
        path.move(to: points[Const.pathEnteringPoint])
        var currentPointIndex = Const.pathEnteringPoint
        // adds lines for each point till reaches initial value
        repeat {
            // finds next point with modulo operator for it to be in range of points
            currentPointIndex = (currentPointIndex + step) % (points.count)
            path.addLine(to: points[currentPointIndex])
        } while currentPointIndex % points.count != Const.pathEnteringPoint

        return path
    }

    // this method returns generic CGRect of set constant size in given origin
    private func setSizeRect(origin: CGPoint) -> CGRect {
        return CGRect(
            origin: origin,
            size: CGSize(
                width: maxStarSize,
                height: maxStarSize
            )
        )
    }

    // this method is for generating origin in screen boundaries for stars to not draw out of bounds
    private func getRandomOrigin() -> CGPoint {
        return CGPoint(
            x: .random(in: (Const.zeroOriginForStarDrawing...(screenSize.width - maxStarSize))),
            y: .random(in: (Const.zeroOriginForStarDrawing...(screenSize.height - maxStarSize)))
        )
    }

    private func getOffset() -> CGPoint {
        var offset = CGPoint()
        guard screenSize.height > screenSize.width else {
            offset.x = (screenSize.width - screenSize.height) / Const.halfSizeDivisionRate
            return offset
        }
        offset.y = (screenSize.height - screenSize.width) / Const.halfSizeDivisionRate
        return offset
    }
}
