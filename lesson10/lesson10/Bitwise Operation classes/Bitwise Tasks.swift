//
//  Bitwise Tasks.swift
//  lesson10
//
//  Created by Harbros 66 on 26.05.21.
//

import Foundation

class Lesson10BitwiseTask {
    var studentGroup = StudentGroup()
    var studentGroupTech: StudentGroup?
    var studentGroupBio: StudentGroup?

    func taskTest() {
        studentGroup.fillWithNStudents(numberOfStudents: 10)
        divisionTask()
        print("There are \(studentGroup.countDevelopers()) programmers in group")
        studentGroup.biologyIsGone()
        print(integerToBitRepresentation(integer: 312412))
    }

    func divisionTask() {
        divideGroup()
        print("Students in Technical subgroup")
        studentGroupTech?.shorthandParseGroup()
        print("Students in Biology subgroup")
        studentGroupBio?.shorthandParseGroup()
    }

    private func divideGroup() {
        studentGroupBio = studentGroup.selectSubGroupBySubjects(
            mainSubject: .subjectBiology,
            subSubject: .subjectAnatomy
        )
        studentGroupTech = studentGroup.selectSubGroupBySubjects(
            mainSubject: .subjectMath,
            subSubject: .subjectEngineering
        )
    }

    func integerToBitRepresentation(integer: Int = .random(in: 0...Int.max)) -> String {
        var bitstring = ""
        var integerCounted = integer
        while integerCounted > 0 {
            let lastbit = integerCounted & 0b1
            bitstring = String(lastbit) + bitstring
            integerCounted >>= 1
        }
        return bitstring
    }
}
