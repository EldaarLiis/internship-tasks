//
//  Student.swift
//  lesson10
//
//  Created by Harbros 66 on 26.05.21.
//

import Foundation

enum StudySubject: Int {
    case subjectBiology     = 0b0000001
    case subjectMath        = 0b0000010
    case subjectDevelopment = 0b0000100
    case subjectEngineering = 0b0001000
    case subjectArt         = 0b0010000
    case subjectPsychology  = 0b0100000
    case subjectAnatomy     = 0b1000000
}

class Student {
    var subjects: Int

    init() {
        subjects = Int.random(in: 1..<256)
    }

    func outputSubjects(studentNumber: Int) {
        print(
            String(
            """
            Student \(studentNumber) studies:
            Biology = \(checkSubject(subject: StudySubject.subjectBiology))
            Math = \(checkSubject(subject: StudySubject.subjectMath))
            Development = \(checkSubject(subject: StudySubject.subjectDevelopment))
            Engineering = \(checkSubject(subject: StudySubject.subjectEngineering))
            Art = \(checkSubject(subject: StudySubject.subjectArt))
            Psychology = \(checkSubject(subject: StudySubject.subjectPsychology))
            Anatomy = \(checkSubject(subject: StudySubject.subjectAnatomy))
            """
            )
        )
    }

    func checkSubject(subject: StudySubject) -> Bool {
        return (self.subjects & subject.rawValue) != 0
    }

    func deleteBiology(studentNumber: Int) {
        if checkSubject(subject: StudySubject.subjectBiology) {
            // i could do xor and it would be the same i guess
            // subjects ^= StudySubject.subjectBiology.rawValue
            subjects &= ~StudySubject.subjectBiology.rawValue
            print(
                "Student number \(studentNumber) is ",
                Bool.random() ? "Happy" : "Sad",
                " about Biology cancellation."
            )
        }
    }
}
