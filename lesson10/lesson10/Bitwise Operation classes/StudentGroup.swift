//
//  StudentGroup.swift
//  lesson10
//
//  Created by Harbros 66 on 28.05.21.
//

import Foundation

struct NumeratedStudent {
    var number: Int
    var student: Student
}

class StudentGroup {
    var students = [NumeratedStudent]()

    func fillWithNStudents(numberOfStudents: Int) {
        (1...numberOfStudents).forEach { number in
            students.append(NumeratedStudent(number: number, student: Student()))
        }
    }

    func addStudent(number: Int, student: Student) {
        students.append(NumeratedStudent(number: number, student: student))
    }

    func biologyIsGone() {
        students.forEach {
            $0.student.deleteBiology(studentNumber: $0.number)
        }
    }

    func countDevelopers() -> Int {
        return students.filter {
            $0.student.checkSubject(subject: StudySubject.subjectEngineering)
        }.count
    }

    func parseGroup() {
        students.forEach {
            $0.student.outputSubjects(studentNumber: $0.number)
        }
    }

    func shorthandParseGroup () {
        students.forEach {
            print($0.number)
        }
    }

    func selectSubGroupBySubjects(mainSubject: StudySubject, subSubject: StudySubject) -> StudentGroup {
        let newStudentGroup = StudentGroup()
        students.forEach {
            if $0.student.checkSubject(subject: mainSubject) ||
                $0.student.checkSubject(subject: subSubject) {
                newStudentGroup.addStudent(number: $0.number, student: $0.student)
            }
        }
        return newStudentGroup
    }
}
