//
//  ViewController.swift
//  lesson10
//
//  Created by Harbros 66 on 25.05.21.
//

import UIKit

class ViewController: UIViewController {

    // let l10NotificationTask = Lesson10NotificationTask()
    // var l10SceneDelegateNotificationTask = SceneDelegateNotificationsListener()
    let l10BitwiseTask = Lesson10BitwiseTask()

    override func viewDidLoad() {
        super.viewDidLoad()
        // l10NotificationTask.taskTest()
        l10BitwiseTask.taskTest()
    }

}
