//
//  Custom Notification listener.swift
//  lesson10
//
//  Created by Harbros 66 on 26.05.21.
//

import UIKit

class SceneDelegateNotificationsListener {
    init() {
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(sceneDidBecomeActive),
            name: UIApplication.didBecomeActiveNotification,
            object: nil
        )
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(sceneWillResignActive),
            name: UIApplication.willResignActiveNotification,
            object: nil
        )
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(sceneWillEnterForeground),
            name: UIScene.willEnterForegroundNotification,
            object: nil
        )
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(sceneDidEnterBackground),
            name: UIScene.didEnterBackgroundNotification,
            object: nil
        )
    }

    @objc func sceneDidBecomeActive() {
        print("Notification: sceneDidBecomeActive")
    }

    @objc func sceneWillResignActive() {
        print("Notification: sceneWillResignActive")
    }

    @objc func sceneWillEnterForeground() {
        print("Notification: sceneWillEnterForeground")
    }

    @objc func sceneDidEnterBackground() {
        print("Notification: sceneDidEnterBackground")
    }

}
