//
//  Notification Classes.swift
//  lesson10
//
//  Created by Harbros 66 on 25.05.21.
//

import UIKit

class Doctor: governmentListenerProtocol {
    var doctorSalary: Double
    var averagePrice: Double

    init(salary: Double, averagePrice: Double) {
        doctorSalary = salary
        self.averagePrice = averagePrice
    }

    func addNotification() {
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(salaryChanged),
            name: .governmentSalaryChangeNotification,
            object: nil
        )

        NotificationCenter.default.addObserver(
            self,
            selector: #selector(averagePriceChanged),
            name: .governmentAveragePriceChangeNotification,
            object: nil
        )
    }

    @objc func salaryChanged(notification: Notification) {
        if let newSalary = notification.userInfo?.values.first as? Double {
            doctorSalary = newSalary
        }
        reevaluateLifeDecisions()
    }

    @objc func averagePriceChanged(_ notification: Notification) {
        if let newAveragePrice = notification.userInfo?.values.first as? Double {
            averagePrice = newAveragePrice
        }
        reevaluateLifeDecisions()
    }

    func reevaluateLifeDecisions() {
        let lifeCost = 30.0 * averagePrice
        if doctorSalary < 0.5 * lifeCost {
            print("Doctor: I hope there are some leftovers in the canteen")
        } else if doctorSalary < lifeCost {
            print("Doctor: Eh... I'm on diet again. Reminds me of those Covid days...")
        } else if doctorSalary == lifeCost {
            print("Doctor: Why does it feel fishy.")
        } else {
            print("Doctor: Wow they really caught me. I've never seen that much money in my life.")
        }
    }

    func addSleepNotification() {
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(goingAsleep),
            name: UIApplication.willResignActiveNotification,
            object: nil
        )
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(wake),
            name: UIApplication.willEnterForegroundNotification,
            object: nil)
    }

    @objc func goingAsleep() {
        print("Doctor has a night shift.")
    }

    @objc func wake() {
        print("Doctor ended night shift with a terrible sleep deprivation")
    }
}
