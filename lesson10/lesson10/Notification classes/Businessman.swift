//
//  Businessman.swift
//  lesson10
//
//  Created by Harbros 66 on 25.05.21.
//

import UIKit

class Businessman: governmentListenerProtocol {
    var incomeTax: Double
    var averagePrice: Double
    let averageIncome = 1800.0

    init(incomeTax: Double, averagePrice: Double) {
        self.incomeTax = incomeTax
        self.averagePrice = averagePrice
    }

    func addNotification() {
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(incomeTaxChanged),
            name: .governmentIncomeTaxChangeNotification,
            object: nil
        )

        NotificationCenter.default.addObserver(
            self,
            selector: #selector(averagePriceChanged),
            name: .governmentAveragePriceChangeNotification,
            object: nil
        )
    }

    @objc func incomeTaxChanged(_ notification: Notification) {
        if let newIncomeTax = notification.userInfo?.values.first as? Double {
            incomeTax = newIncomeTax
        }
        reevaluateLifeDecisions()
    }

    @objc func averagePriceChanged(_ notification: Notification) {
        if let newAveragePrice = notification.userInfo?.values.first as? Double {
            averagePrice = newAveragePrice
        }
        reevaluateLifeDecisions()
    }

    func reevaluateLifeDecisions() {
        let lifeCost = 30.0 * averagePrice
        let realIncome = (1 - incomeTax / 100) * averageIncome
        if realIncome < 0.5 * lifeCost {
            print("Businessman: Bitcoin was a bad decision.")
        } else if realIncome < lifeCost {
            print("Businessman: I guess i have a stash. It won't be that hard.")
        } else if realIncome == lifeCost {
            print("Businessman: Prices are really high... Is my grandma ok?")
        } else {
            print("Businessman: Haha. Classics.")
        }
    }

    func addSleepNotification() {
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(goingAsleep),
            name: UIApplication.willResignActiveNotification,
            object: nil
        )
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(wake),
            name: UIApplication.willEnterForegroundNotification,
            object: nil)
    }

    @objc func goingAsleep() {
        print("Businessman is going asleep")
    }

    @objc func wake() {
        print("New day - new opportunities for business")
    }
}
