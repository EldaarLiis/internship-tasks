//
//  Pensioneer.swift
//  lesson10
//
//  Created by Harbros 66 on 25.05.21.
//

import UIKit

class Pensioneer: governmentListenerProtocol {
    var pension: Double
    var averagePrice: Double

    init(pension: Double, averagePrice: Double) {
        self.pension = pension
        self.averagePrice = averagePrice
    }

    func addNotification() {
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(pensionChanged),
            name: .governmentPensionChangeNotification,
            object: nil
        )

        NotificationCenter.default.addObserver(
            self,
            selector: #selector(averagePriceChanged),
            name: .governmentAveragePriceChangeNotification,
            object: nil
        )
    }

    @objc func pensionChanged(_ notification: Notification) {
        if let newPension = notification.userInfo?.values.first as? Double {
            pension = newPension
        }
        reevaluateLifeDecisions()
    }

    @objc func averagePriceChanged(_ notification: Notification) {
        if let newAveragePrice = notification.userInfo?.values.first as? Double {
            averagePrice = newAveragePrice
        }
        reevaluateLifeDecisions()
    }

    func reevaluateLifeDecisions() {
        let lifeCost = 30.0 * averagePrice
        if pension < 0.5 * lifeCost {
            print("Pensioneer: Where is my dear grandchild...")
        } else if pension < lifeCost {
            print("Pensioneer: No electricity next month. As in my youth...")
        } else if pension == lifeCost {
            print("Pensioneer: Next day achieved.")
        } else {
            print("Pensioneer: Bitcoin was a good decision...")
        }
    }

    func addSleepNotification() {
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(goingAsleep),
            name: UIApplication.willResignActiveNotification,
            object: nil
        )
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(wake),
            name: UIApplication.willEnterForegroundNotification,
            object: nil)
    }

    @objc func goingAsleep() {
        print("Pensioneer is going asleep")
    }

    @objc func wake() {
        print("Pensioneer has a new day to live.")
    }
}
