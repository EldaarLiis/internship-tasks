//
//  Government.swift
//  lesson10
//
//  Created by Harbros 66 on 25.05.21.
//

import Foundation

protocol governmentListenerProtocol {
    func addNotification()
    func addSleepNotification()
    func reevaluateLifeDecisions()
}

class Government {
    var incomeTax: Double {
        didSet {
            NotificationCenter.default.post(
                name: .governmentIncomeTaxChangeNotification,
                object: nil,
                userInfo: ["Income Tax": incomeTax]
            )
        }
    }
    var pension: Double {
        didSet {
            NotificationCenter.default.post(
                name: .governmentPensionChangeNotification,
                object: nil,
                userInfo: ["Pension": pension]
            )
        }
    }
    var salary: Double {
        didSet {
            NotificationCenter.default.post(
                name: .governmentSalaryChangeNotification,
                object: nil,
                userInfo: ["Salary": salary]
            )
        }
    }
    var averagePrice: Double {
        didSet {
            NotificationCenter.default.post(
                name: .governmentAveragePriceChangeNotification,
                object: nil,
                userInfo: ["Average Price": averagePrice]
            )
        }
    }

    init(incomeTax: Double, pension: Double, salary: Double, averagePrice: Double) {
        self.incomeTax = incomeTax
        self.pension = pension
        self.salary = salary
        self.averagePrice = averagePrice
    }

    convenience init() {
        self.init(incomeTax: 10, pension: 700, salary: 1200, averagePrice: 20)
    }
}
