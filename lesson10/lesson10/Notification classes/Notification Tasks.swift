//
//  Notification Tasks.swift
//  lesson10
//
//  Created by Harbros 66 on 25.05.21.
//

import Foundation

class Lesson10NotificationTask {
    var government: Government
    var doctor: Doctor
    var businessman: Businessman
    var pensioneer: Pensioneer

    init() {
        government = Government(incomeTax: 25, pension: 500, salary: 800, averagePrice: 20)
        doctor = Doctor(salary: government.salary, averagePrice: government.averagePrice)
        businessman = Businessman(incomeTax: government.incomeTax, averagePrice: government.averagePrice)
        pensioneer = Pensioneer(pension: government.pension, averagePrice: government.averagePrice)
    }

    func taskTest() {
        let city: [governmentListenerProtocol] = [doctor, businessman, pensioneer]
        city.forEach {
            $0.addSleepNotification()
            $0.addNotification()
        }
        government.averagePrice = 30
    }
}

extension Notification.Name {
    static let governmentIncomeTaxChangeNotification =
        Notification.Name(rawValue: "governmentIncomeTaxChangeNotification")
    static let governmentSalaryChangeNotification =
        Notification.Name(rawValue: "governmentSalaryChangeNotification")
    static let governmentPensionChangeNotification =
        Notification.Name(rawValue: "governmentPensionChangeNotification")
    static let governmentAveragePriceChangeNotification =
        Notification.Name(rawValue: "governmentAveragePriceChangeNotification")
}
