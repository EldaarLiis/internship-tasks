//
//  ViewController.swift
//  Lesson20_01
//
//  Created by Harbros 66 on 12.06.21.
//

import UIKit

private enum Const {
    static let cellIdentifier = "id"
    static let errorHeader = "Error"
}

class ViewController: UIViewController, UITableViewDataSource {
    private let tableView = UITableView()
    private let sectionManager = TableSectionManager()

    override func viewDidLoad() {
        super.viewDidLoad()
        addSubviews()
        tableView.dataSource = self
    }

    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        tableView.frame = view.safeAreaLayoutGuide.layoutFrame
    }

    private func addSubviews() {
        view.backgroundColor = .white
        tableView.backgroundColor = .white
        view.addSubview(tableView)
    }

    // MARK: - UITableViewDataSource
    func numberOfSections(in tableView: UITableView) -> Int {
        return sectionManager.tableSections.count
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let currSection = sectionManager.tableSections.safe(at: section) else { return .zero }
        return currSection.sectionContent.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let model = sectionManager.tableSections.safe(at: indexPath.section)?.sectionContent.safe(at: indexPath.row)
        guard let cellModel = model else { return UITableViewCell() }

        switch cellModel {
        case let .color(color):
            let cell = UITableViewCell(style: .default, reuseIdentifier: Const.cellIdentifier)
            cell.backgroundColor = color.backgroundColor
            cell.textLabel?.text = color.textLabelText
            return cell
        case let .student(student):
            let cell = UITableViewCell(style: .value1, reuseIdentifier: Const.cellIdentifier)
            cell.textLabel?.text = student.textLabelText
            cell.detailTextLabel?.text = student.detailTextLabelText
            cell.textLabel?.textColor = student.textLabelColor
            return cell
        }
    }

    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        guard let currSection = sectionManager.tableSections.safe(at: section) else { return Const.errorHeader }
        return currSection.sectionName
    }
}
