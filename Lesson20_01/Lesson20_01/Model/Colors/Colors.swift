//
//  Colors.swift
//  Lesson20_01
//
//  Created by Harbros 66 on 12.06.21.
//

import UIKit

extension UIColor {
    static var randomUIColor: UIColor {
        return Self(
            red: CGFloat.random(in: 0.0...1.0),
            green: CGFloat.random(in: 0.0...1.0),
            blue: CGFloat.random(in: 0.0...1.0),
            alpha: 1
        )
    }
    var rgbName: String {
        let selfCIColor = CIColor(color: self)
        let red = Int(selfCIColor.red * 255)
        let green = Int(selfCIColor.green * 255)
        let blue = Int(selfCIColor.blue * 255)
        return String("RGB(\(red), \(green), \(blue)) \(self.accessibilityName)").capitalized
    }
}

class Color {
    var backgroundColor: UIColor? = .randomUIColor
    var textLabelText: String {
        guard let cl = self.backgroundColor else { return String() }
        return cl.rgbName
    }

    var textLabelColor: UIColor?
    var detailTextLabelText: String?
}
