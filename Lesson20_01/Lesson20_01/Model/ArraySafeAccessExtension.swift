//
//  ArraySafeAccessExtension.swift
//  Lesson20_01
//
//  Created by Harbros 66 on 28.06.21.
//

import Foundation

extension Array {
    func safe(at index: Int) -> Element? {
        guard self.indices.contains(index) else { return nil }
        return self[index]
    }
}
