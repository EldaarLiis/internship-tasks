//
//  Group.swift
//  Lesson20_01
//
//  Created by Harbros 66 on 17.06.21.
//

import Foundation

class MarkedGroup: TableSection {
    var sectionName: String
    var sectionContent: [CellType]

    init(markWordDescription: String, groupOfStudents: [Student]) {
        sectionName = markWordDescription
        sectionContent = [CellType]()
        groupOfStudents.forEach { student in
            sectionContent.append(.student(student))
        }
    }
}
