//
//  GroupWorkings.swift
//  Lesson20_01
//
//  Created by Harbros 66 on 17.06.21.
//

import Foundation

private enum Const {
    static let numberOfStudents = 30
}

class StudentGroup {
    func randomMarkedGroup() -> [MarkedGroup] {
        var group = [Student]()
        (1...Const.numberOfStudents).forEach { _ in
            group.append(Student())
        }
        group.sort {
            $0.textLabelText < $1.textLabelText
        }
        var setOfMarks = Set<Mark>()
        var markedGroupArray = [MarkedGroup]()
        group.forEach { student in
            setOfMarks.insert(student.midMark)
        }
        setOfMarks.forEach { mark in
            let groupOfThisMark = MarkedGroup(
                markWordDescription: mark.description,
                groupOfStudents: group.filter { $0.midMark == mark }
            )
            markedGroupArray.append(groupOfThisMark)
        }
        return markedGroupArray
    }
}
