//
//  UpdateProtocol.swift
//  Lesson26
//
//  Created by Harbros 66 on 12.07.21.
//

import RealmSwift

protocol Updateable {
    func updateWithObject(newObject: Object)
}
