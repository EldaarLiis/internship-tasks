//
//  TeacherModel.swift
//  Lesson26
//
//  Created by Harbros 66 on 7.07.21.
//

import RealmSwift

@objcMembers
class Teacher: Object, Updateable {
    dynamic var id: String = UUID().uuidString
    dynamic var name = ""
    dynamic var lastname = ""
    let courses = List<Course>()

    func updateWithObject(newObject newTeacher: Object) {
        guard let newTeacher = newTeacher as? Teacher else { return }
        name = newTeacher.name
        lastname = newTeacher.lastname
    }
}
