//
//  StudentModel.swift
//  Lesson26
//
//  Created by Harbros 66 on 7.07.21.
//

import RealmSwift

@objcMembers
class Student: Object, Updateable {
    dynamic var id: String = UUID().uuidString
    dynamic var name = ""
    dynamic var lastname = ""
    dynamic var mail = ""
    let courses = List<Course>()

    func updateWithObject(newObject newStudent: Object) {
        guard let newStudent = newStudent as? Student else { return }
        name = newStudent.name
        lastname = newStudent.lastname
        mail = newStudent.mail
    }
}
