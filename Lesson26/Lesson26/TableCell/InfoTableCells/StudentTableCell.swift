//
//  StudentTableCell.swift
//  Lesson26
//
//  Created by Harbros 66 on 7.07.21.
//

import UIKit

private enum Const {
    static let half: CGFloat = 0.5
}

class StudentTableCell: UITableViewCell {
    let nameLabel = UILabel()
    let mailLabel = UILabel()

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: .default, reuseIdentifier: reuseIdentifier)
        self.separatorInset = .zero
        addSubview(nameLabel)
        addSubview(mailLabel)
        configureLabels()
    }

    func configure(for model: Student) {
        labelLabels(with: model)
    }

    private func labelLabels(with model: Student) {
        nameLabel.text = "\(model.name.capitalized)  \(model.lastname.capitalized)"
        mailLabel.text = model.mail.capitalized
    }

    private func configureLabels() {
        nameLabel.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            nameLabel.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: GConst.cellLabelXOffset),
            nameLabel.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -GConst.cellLabelXOffset),
            nameLabel.topAnchor.constraint(equalTo: topAnchor),
            nameLabel.bottomAnchor.constraint(equalTo: centerYAnchor)
        ])
        nameLabel.adjustsFontSizeToFitWidth = true

        mailLabel.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            mailLabel.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: GConst.cellLabelXOffset),
            mailLabel.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -GConst.cellLabelXOffset),
            mailLabel.topAnchor.constraint(equalTo: contentView.centerYAnchor),
            mailLabel.bottomAnchor.constraint(equalTo: contentView.bottomAnchor)
        ])
        mailLabel.adjustsFontSizeToFitWidth = true
    }

    required init?(coder: NSCoder) { fatalError("init(coder:) has not been implemented") }
}
