//
//  StudentTableCell.swift
//  Lesson26
//
//  Created by Harbros 66 on 7.07.21.
//

import UIKit

private enum Const {
    static let half: CGFloat = 0.5
}

class TeacherTableCell: UITableViewCell {
    let nameLabel = UILabel()
    let courcesCell = UILabel()

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: .default, reuseIdentifier: reuseIdentifier)
        self.separatorInset = .zero
        addSubview(nameLabel)
        addSubview(courcesCell)
        configureLabels()
    }

    func configure(for model: Teacher) {
        labelLabels(with: model)
    }

    private func labelLabels(with model: Teacher) {
        nameLabel.text = "\(model.name.capitalized)  \(model.lastname.capitalized)"
        courcesCell.text = "\(GConst.coursesSectionTitle): \(model.courses.count.description.capitalized)"
    }

    private func configureLabels() {
        nameLabel.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            nameLabel.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: GConst.cellLabelXOffset),
            nameLabel.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -GConst.cellLabelXOffset),
            nameLabel.topAnchor.constraint(equalTo: topAnchor),
            nameLabel.bottomAnchor.constraint(equalTo: centerYAnchor)
        ])
        nameLabel.adjustsFontSizeToFitWidth = true

        courcesCell.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            courcesCell.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: GConst.cellLabelXOffset),
            courcesCell.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -GConst.cellLabelXOffset),
            courcesCell.topAnchor.constraint(equalTo: centerYAnchor),
            courcesCell.bottomAnchor.constraint(equalTo: bottomAnchor)
        ])
        courcesCell.adjustsFontSizeToFitWidth = true
    }

    required init?(coder: NSCoder) { fatalError("init(coder:) has not been implemented") }
}
