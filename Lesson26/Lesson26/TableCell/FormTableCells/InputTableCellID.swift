//
//  InputTableCellID.swift
//  Lesson26
//
//  Created by Harbros 66 on 9.07.21.
//

import Foundation

enum InputTableCellID: String {
    case name
    case lastname
    case mail
    case field

    func checkCorrect(_ string: String?) -> Bool {
        guard let toCheck = string else { return false }
        switch self {
        case .name, .lastname, .field:
            return toCheck.isName && !toCheck.isEmpty
        case .mail:
            return toCheck.isEmail && !toCheck.isEmpty
        }
    }
}
