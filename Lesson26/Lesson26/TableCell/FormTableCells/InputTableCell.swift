//
//  InputTableCell.swift
//  Lesson26
//
//  Created by Harbros 66 on 8.07.21.
//

import UIKit

class InputTableCell: UITableViewCell {
    private let label = UILabel()
    private let textField = UITextField()
    var type: InputTableCellID?
    var textFieldDelegate: UITextFieldDelegate? {
        get { textField.delegate }
        set { textField.delegate = newValue }
    }
    func checkCorrect() -> Bool {
        return (type?.checkCorrect(textField.text) ?? false)
    }
    var textFieldValue: String? {
        get { textField.text }
        set { textField.text = newValue}
    }

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: .default, reuseIdentifier: reuseIdentifier)
        self.separatorInset = .zero
    }

    func configure(for inputText: String) {
        contentView.addSubview(label)
        contentView.addSubview(textField)
        type = .init(rawValue: inputText)
        labelItems(with: inputText)
        configureItems()
    }

    private func labelItems(with inputText: String) {
        label.text = inputText.capitalized
        label.textColor = .gray
        textField.placeholder = inputText.capitalized
    }

    private func configureItems() {
        label.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            label.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: GConst.cellLabelXOffset),
            label.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -GConst.cellLabelXOffset),
            label.topAnchor.constraint(equalTo: topAnchor),
            label.bottomAnchor.constraint(equalTo: centerYAnchor)
        ])
        label.adjustsFontSizeToFitWidth = true

        textField.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            textField.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: GConst.cellLabelXOffset),
            textField.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -GConst.cellLabelXOffset),
            textField.topAnchor.constraint(equalTo: centerYAnchor),
            textField.bottomAnchor.constraint(equalTo: bottomAnchor)
        ])
        textField.adjustsFontSizeToFitWidth = true
        textField.autocorrectionType = .no
        textField.autocapitalizationType = .none
    }

    required init?(coder: NSCoder) { fatalError("init(coder:) has not been implemented") }
}
