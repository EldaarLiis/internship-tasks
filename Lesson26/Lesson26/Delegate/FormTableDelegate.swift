//
//  FormTableDelegate.swift
//  Lesson26
//
//  Created by Harbros 66 on 9.07.21.
//

import RealmSwift

class FormTableDelegate<T: Object>: NSObject, UITableViewDelegate {
    private weak var DBProvider: DBProviderForFormTable?
    init(provider: DBProviderForFormTable) {
        DBProvider = provider
    }

    func tableView(_ tableView: UITableView, willSelectRowAt indexPath: IndexPath) -> IndexPath? {
        switch T.self {
        case is Course.Type:
            guard indexPath.section != .zero else { return nil }
            return indexPath
        default:
            return nil
        }
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let isTeacherAssigned = (DBProvider?.checkTeacher() ?? false)
        guard
            indexPath == GConst.courseFormStudentIndexPath ||
            (indexPath == GConst.courseFormTeacherIndexPath && !isTeacherAssigned)
        else {
            DBProvider?.openObject(indexPath)
            return
        }
        DBProvider?.addObject(indexPath.section)
    }

    func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCell.EditingStyle {
        guard indexPath.section != .zero else { return .none }
        let isTeacherAssigned = (DBProvider?.checkTeacher() ?? false)
        guard
            indexPath == GConst.courseFormStudentIndexPath ||
            (indexPath == GConst.courseFormTeacherIndexPath && !isTeacherAssigned)
        else { return .delete }
        return .none
    }
}
