//
//  InfoTableSelectionDataSource.swift
//  Lesson26
//
//  Created by Harbros 66 on 11.07.21.
//

import RealmSwift

class InfoTableSelectionDataSource<T: Object>: InfoTableDataSource<T> {
    override func configureStudentCell(_ cell: UITableViewCell, at indexPath: IndexPath) -> UITableViewCell {
        guard
            let studentCell = cell as? StudentTableCell,
            let studentAtIndex = DBProvider?.getItem(indexPath.row) as? Student
        else { return UITableViewCell() }
        studentCell.configure(for: studentAtIndex)
        studentCell.accessoryType = (DBProvider?.checkIfOnCourse(indexPath.row) ?? true) ? .checkmark : .none
        return studentCell
    }
}
