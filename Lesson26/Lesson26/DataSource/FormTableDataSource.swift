//
//  FormTableDataSource.swift
//  Lesson26
//
//  Created by Harbros 66 on 8.07.21.
//

import RealmSwift

class FormTableDataSource<T: Object>: NSObject, UITableViewDataSource {
    private weak var DBProvider: DBProviderForFormTable?

    let sectionManager: FormTableSectionManager<T>
    init(sectionManager: FormTableSectionManager<T>, provider: DBProviderForFormTable) {
        self.sectionManager = sectionManager
        DBProvider = provider
    }

    func numberOfSections(in tableView: UITableView) -> Int {
        return sectionManager.sectionNumber
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return sectionManager.rowCount(for: section)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard indexPath.section != .zero else {
            let cell = tableView.dequeueReusableCell(withIdentifier: GConst.inputCellID, for: indexPath)
            return configureInfoCell(cell, at: indexPath)
        }
        guard indexPath.section != 1 else {
            switch T.self {
            case is Student.Type, is Teacher.Type:
                let cell = tableView.dequeueReusableCell(withIdentifier: GConst.courcesCellID, for: indexPath)
                return configureCoursesCell(cell, at: indexPath)
            case is Course.Type:
                let cell = tableView.dequeueReusableCell(withIdentifier: GConst.addCellID, for: indexPath)
                return configureTeacherCell(cell, at: indexPath)
            default:
                return UITableViewCell()
            }
        }
        let cell = tableView.dequeueReusableCell(withIdentifier: GConst.addCellID, for: indexPath)
        return configureStudentCell(cell, at: indexPath)
    }

    private func configureCoursesCell(_ cell: UITableViewCell, at indexPath: IndexPath) -> UITableViewCell {
        guard
            let courseCell = cell as? CourseTableCell,
            let course = sectionManager.courses?.safe(at: indexPath.row)
        else {
            return UITableViewCell()
        }
        courseCell.configure(for: course)
        return courseCell
    }

    private func configureTeacherCell(_ cell: UITableViewCell, at indexPath: IndexPath) -> UITableViewCell {
        guard let teacher = (sectionManager.model as? Course)?.teacher else {
            cell.textLabel?.text = "Add Teacher"
            cell.textLabel?.textColor = .black
            return cell
        }
        let teacherCell = TeacherTableCell()
        teacherCell.configure(for: teacher)
        return teacherCell
    }

    private func configureStudentCell(_ cell: UITableViewCell, at indexPath: IndexPath) -> UITableViewCell {
        guard indexPath.row != .zero else {
            cell.textLabel?.text = "Add Student"
            cell.textLabel?.textColor = .black
            return cell
        }
        guard let student = (sectionManager.model as? Course)?.students.safe(at: indexPath.row - 1) else {
            return cell
        }
        let studentCell = StudentTableCell()
        studentCell.configure(for: student)
        return studentCell
    }

    private func configureInfoCell(_ cell: UITableViewCell, at indexPath: IndexPath) -> UITableViewCell {
        guard let inputCell = cell as? InputTableCell else {
            return UITableViewCell()
        }
        inputCell.configure(for: sectionManager.classProperties[indexPath.row])
        
        guard
            let model = sectionManager.model,
            let text = model.value(forKey: sectionManager.classProperties[indexPath.row]) as? String
        else { return inputCell }
        inputCell.textFieldValue = text
        return inputCell
    }

    func tableView(
        _ tableView: UITableView,
        commit editingStyle: UITableViewCell.EditingStyle,
        forRowAt indexPath: IndexPath
    ) {
        guard editingStyle == .delete else { return }
        switch indexPath.section {
        case GConst.courseFormTeacherIndexPath.section:
            DBProvider?.deleteTeacher()
        case GConst.courseFormStudentIndexPath.section:
            DBProvider?.deleteStudent(indexPath)
        default:
            return
        }
    }

    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return sectionManager.title(for: section)
    }
}
