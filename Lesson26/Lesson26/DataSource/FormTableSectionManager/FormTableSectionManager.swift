//
//  FormTableSectionManager.swift
//  Lesson26
//
//  Created by Harbros 66 on 8.07.21.
//

import RealmSwift

class FormTableSectionManager<T: Object> {
    let classProperties = T.classProperties()
    let model: T?
    init(model: T?) { self.model = model }

    var sectionNumber: Int {
        switch T.self {
        case is Student.Type:
            guard
                let model = model as? Student,
                !model.courses.isEmpty
            else { return 1 }
            return 2
        case is Teacher.Type:
            guard
                let model = model as? Teacher,
                !model.courses.isEmpty
            else { return 1 }
            return 2
        case is Course.Type:
            guard model != nil else { return 1 }
            return 3
        default:
            return .zero
        }
    }

    func rowCount(for section: Int) -> Int {
        guard section != .zero else { return classProperties.count }
        switch T.self {
        case is Student.Type:
            return (model as? Student)?.courses.count ?? .zero
        case is Teacher.Type:
            return (model as? Teacher)?.courses.count ?? .zero
        case is Course.Type:
            if section == 1 {
                return 1
            } else {
                let studentCount = (model as? Course)?.students.count ?? .zero
                return studentCount + 1
            }
        default:
            return .zero
        }
    }

    var courses: [Course]? {
        switch T.self {
        case is Student.Type:
            guard let model = model as? Student else { return nil }
            return Array(model.courses)
        case is Teacher.Type:
            guard let model = model as? Teacher else { return nil }
            return Array(model.courses)
        default:
            return nil
        }
    }

    func title(for section: Int) -> String {
        switch T.self {
        case is Student.Type, is Teacher.Type:
            switch section {
            case GConst.firstSectionIndexPath.section:
                return GConst.selfInfoSectionTitle
            case GConst.secondSectionIndexPath.section:
                return GConst.coursesSectionTitle
            default:
                return .init()
            }
        case is Course.Type:
            switch section {
            case GConst.firstSectionIndexPath.section:
                return GConst.courseSectionTitle
            case GConst.secondSectionIndexPath.section:
                return GConst.teacherSectionTitle
            case GConst.thirdSectionIndexPath.section:
                return GConst.studentSectionTitle
            default:
                return .init()
            }
        default:
            return .init()
        }
    }
}
