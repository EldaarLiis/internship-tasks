//
//  DBProviderForInfoTable.swift
//  Lesson26
//
//  Created by Harbros 66 on 12.07.21.
//

import RealmSwift

protocol DBProviderForInfoTable: AnyObject {
    func getItem(_ atIndex: Int) -> Object?
    func getNumberOfItems() -> Int
    func checkIfOnCourse(_ atIndex: Int) -> Bool
    func openObject(_ atIndex: Int?) -> ()
    func deleteObject(_ atIndex: Int) -> ()

    func addTeacher(_ atIndex: Int) -> ()
    func addStudent(_ atIndex: Int) -> ()
}
