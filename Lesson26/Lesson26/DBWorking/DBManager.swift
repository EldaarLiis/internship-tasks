//
//  DBManager.swift
//  Lesson26
//
//  Created by Harbros 66 on 7.07.21.
//

import RealmSwift

class DBManager<T: Object> {
    private let realm = try! Realm()

    func fetchItems() -> [T] {
        let fetchedItems = realm.objects(T.self)
        return Array(fetchedItems)
    }

    func addObjectByDictionary(oldObject: Object?, newObject: Object) {
        guard let oldObject = oldObject else {
            addObject(object: newObject)
            return
        }
        updateObject(oldObject: oldObject, newObject: newObject)
    }

    func addObject(object: Object) {
        try? realm.write{
            realm.add(object)
        }
    }

    func updateObject(oldObject: Object, newObject: Object) {
        guard let updateableObject = oldObject as? Updateable else { return }
        try? realm.write {
            updateableObject.updateWithObject(newObject: newObject)
        }
    }

    func deleteObject(object: Object?) {
        guard let object = object else { return }
        try? realm.write {
            configureDeletionDependencies(for: object)
            realm.delete(object)
        }
    }

    func configureDeletionDependencies(for object: Object) {
        if let student = object as? Student {
            student.courses.forEach {
                $0.students.delete(object: student)
            }
        } else if let teacher = object as? Teacher {
            teacher.courses.forEach {
                $0.teacher = nil
            }
        } else if let course = object as? Course {
            course.students.forEach {
                $0.courses.delete(object: course)
            }
            course.teacher?.courses.delete(object: course)
        }
    }

    func tryAddTeacherToCourse(teacher: Teacher, course: Course) {
        try? realm.write {
            course.teacher?.courses.delete(object: course)
            course.teacher = teacher
            teacher.courses.append(course)
        }
    }

    func tryDeleteTeacherFromCourse(course: Course) {
        try? realm.write {
            course.teacher?.courses.delete(object: course)
            course.teacher = nil
        }
    }

    func tryAddStudentToCourse(student: Student, course: Course) {
        try? realm.write {
            guard student.courses.index(of: course) == nil else {
                student.courses.delete(object: course)
                course.students.delete(object: student)
                return
            }
            student.courses.append(course)
            course.students.append(student)
        }
    }

    func tryDeleteStudentFromCourse(student: Student, course: Course) {
        guard course.students.index(of: student) != nil else { return }
        try? realm.write {
            course.students.delete(object: student)
            student.courses.delete(object: course)
        }
    }
}
