//
//  MainTabBarController.swift
//  Lesson26
//
//  Created by Harbros 66 on 7.07.21.
//

import UIKit

class MainTabBarController: UITabBarController {
    let studentTab: UINavigationController
    let teacherTab: UINavigationController
    let courseTab: UINavigationController
    init() {
        studentTab = .init(rootViewController: InfoTableController<Student>())
        teacherTab = .init(rootViewController: InfoTableController<Teacher>())
        courseTab = .init(rootViewController: InfoTableController<Course>())

        super.init(nibName: nil, bundle: nil)

        configureVisual()
        
        viewControllers = [studentTab, teacherTab, courseTab]
    }

    private func configureVisual() {
        studentTab.tabBarItem.image = .checkmark
        studentTab.tabBarItem.title = "Students"

        teacherTab.tabBarItem.image = .checkmark
        teacherTab.tabBarItem.title = "Teachers"

        courseTab.tabBarItem.image = .checkmark
        courseTab.tabBarItem.title = "Courses"
    }

    required init?(coder: NSCoder) { fatalError("init(coder:) has not been implemented") }
}
