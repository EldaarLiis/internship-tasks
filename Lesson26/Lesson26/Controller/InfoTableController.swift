//
//  InfoTableController.swift
//  Lesson26
//
//  Created by Harbros 66 on 7.07.21.
//

import RealmSwift

class InfoTableController<T: Object>: UIViewController {
    private let DBManager: DBManager<T>
    private var items: [T] = .init()
    private var tableViewDataSource: InfoTableDataSource<T>?
    private var tableViewSelectionDataSourse: InfoTableSelectionDataSource<T>?
    private var tableViewDelegate: InfoTableDelegate<T>?
    private var tableViewSelectionDelegate: InfoTableSelectionDelegate<T>?
    var model: Course?

    private let tableView = UITableView(frame: .zero, style: .insetGrouped)
    private let newItemButton = UIBarButtonItem(systemItem: .add)

    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        DBManager = .init()
        super.init(nibName: nil, bundle: nil)

        tableViewDataSource = .init(provider: self)
        tableViewDelegate = .init(provider: self)
        tableView.delegate = tableViewDelegate
        tableView.dataSource = tableViewDataSource
    }

    init(courseModel: Course) {
        DBManager = .init()
        self.model = courseModel
        super.init(nibName: nil, bundle: nil)
        
        tableViewSelectionDelegate = .init(provider: self)
        tableView.delegate = tableViewSelectionDelegate
        
        tableViewSelectionDataSourse = .init(provider: self)
        tableView.dataSource = tableViewSelectionDataSourse
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        view.addSubview(tableView)

        prepareTableView()
        prepareNavigationBar()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        items = DBManager.fetchItems()
        tableView.reloadData()
    }

    private func prepareTableView() {
        tableView.register(StudentTableCell.self, forCellReuseIdentifier: GConst.studentCellID)
        tableView.register(TeacherTableCell.self, forCellReuseIdentifier: GConst.teacherCellID)
        tableView.register(CourseTableCell.self, forCellReuseIdentifier: GConst.courceCellID)

        addTableViewConstraints()
        tableView.separatorStyle = .singleLine
        tableView.separatorColor = .purple
    }
    private func prepareNavigationBar() {
        navigationItem.title = String(describing: T.self)
        navigationItem.setRightBarButton(newItemButton, animated: true)
        newItemButton.action = #selector(newObject)
        newItemButton.target = self
    }

    private func addTableViewConstraints() {
        tableView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            tableView.topAnchor.constraint(equalTo: view.topAnchor),
            tableView.bottomAnchor.constraint(equalTo: view.bottomAnchor),
            tableView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            tableView.trailingAnchor.constraint(equalTo: view.trailingAnchor)
        ])
    }

    @objc private func newObject() {
        openObject(nil)
    }

    required init?(coder: NSCoder) { fatalError() }
}

extension InfoTableController: DBProviderForInfoTable {
    func getItem(_ atIndex: Int) -> Object? { return items.safe(at: atIndex) }

    func getNumberOfItems() -> Int { return items.count }

    func checkIfOnCourse(_ atIndex: Int) -> Bool {
        guard
            let model = items.safe(at: atIndex),
            let course = self.model
        else { return false }
        switch T.self {
        case is Teacher.Type:
            return (model as? Teacher)?.courses.index(of: course) != nil
        case is Student.Type:
            return (model as? Student)?.courses.index(of: course) != nil
        default:
            return false
        }
    }

    func openObject(_ atIndex: Int?) {
        let model = items.safe(at: atIndex)
        navigationController?.pushViewController(FormTableController<T>.init(for: model), animated: true)
    }

    func deleteObject(_ atIndex: Int) {
        guard let model = items.safe(at: atIndex) else { return }
        DBManager.deleteObject(object: model)
    }

    func addTeacher(_ atIndex: Int) {
        guard
            let course = model,
            T.self is Teacher.Type,
            let teacher = (items as? [Teacher])?.safe(at: atIndex)
        else { return }
        DBManager.tryAddTeacherToCourse(teacher: teacher, course: course)
        navigationController?.popViewController(animated: true)
    }

    func addStudent(_ atIndex: Int) {
        guard
            let course = model,
            T.self is Student.Type,
            let student = (items as? [Student])?.safe(at: atIndex)
        else { return }
        DBManager.tryAddStudentToCourse(student: student, course: course)
    }
}
