//
//  GeneralConstants.swift
//  Lesson26
//
//  Created by Harbros 66 on 7.07.21.
//

import UIKit

enum GConst {
    static let studentCellID = "StudentCellID"
    static let teacherCellID = "TeacherCellID"
    static let courceCellID = "CourseCellID"

    static let inputCellID = "InputCellID"
    static let courcesCellID = "CourcesCellID"
    static let addCellID = "AddCellID"

    static let cellLabelXOffset: CGFloat = 10

    static let errorTitle = "Error"
    static let okTitle = "OK"
    static let errorMessage = "Error in fields:"
    static let emptyMark = "-- empty"

    static let firstSectionIndexPath = IndexPath(row: 0, section: 0)
    static let secondSectionIndexPath = IndexPath(row: 0, section: 1)
    static let thirdSectionIndexPath = IndexPath(row: 0, section: 2)
    static let courseFormTeacherIndexPath = IndexPath(row: 0, section: 1)
    static let courseFormStudentIndexPath = IndexPath(row: 0, section: 2)

    static let selfInfoSectionTitle = "Personal info"
    static let courseSectionTitle = "Course"
    static let coursesSectionTitle = "Courses"
    static let teacherSectionTitle = "Teacher"
    static let studentSectionTitle = "Students"
}
