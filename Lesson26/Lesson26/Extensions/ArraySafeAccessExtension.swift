//
//  ArraySafeAccessExtension.swift
//  Lesson23
//
//  Created by Harbros 66 on 29.06.21.
//

import RealmSwift

extension Array {
    func safe(at index: Int?) -> Element? {
        guard let index = index,
              self.indices.contains(index)
        else { return nil }
        return self[index]
    }
}

extension List {
    func safe(at index: Int?) -> Element? {
        guard let index = index,
              self.indices.contains(index)
        else { return nil }
        return self[index]
    }
}
