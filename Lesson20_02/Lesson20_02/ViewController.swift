//
//  ViewController.swift
//  Lesson20_02
//
//  Created by Harbros 66 on 12.06.21.
//

import UIKit

private enum Const {
    static let cellIdentifier = "id"
    static let numberOfRows = 5
    static let numberOfSections = 4

    static let tableViewHeightOfScreen: CGFloat = 8 / 9
    static let buttonRowHeightOfScreen: CGFloat = 1 - tableViewHeightOfScreen
    static let buttonCount: CGFloat = 3

    static let editButtonPos = 0
    static let addRowButtonPos = 1
    static let addSectionButtonPos = 2

    static let editButtonText = "Edit"
    static let addRowButtonText = "Add row"
    static let addSectionButtonText = "Add section"
    static let errorSectionTitle = "Error"

    static let editButtonBackgroundColor: UIColor = .lightGray
    static let addRowButtonBackgroundColor: UIColor = .green
    static let addSectionButtonBackgroundColor: UIColor = .blue
}

class ViewController: UIViewController, UITableViewDataSource {
    override var prefersStatusBarHidden: Bool {
        return true
    }
    private let tableView = UITableView()
    private let editButton = UIButton()
    private let addRowButton = UIButton()
    private let addSectionButton = UIButton()
    private let sectionManager = TableSectionManager()

    override func viewDidLoad() {
        super.viewDidLoad()
        addSubviews()
        tableView.dataSource = self
        addButtonTarget()
    }

    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        tableView.frame = tableViewRect(view.safeAreaLayoutGuide.layoutFrame)
        editButton.frame = buttonRect(view.safeAreaLayoutGuide.layoutFrame, pos: Const.editButtonPos)
        addRowButton.frame = buttonRect(view.safeAreaLayoutGuide.layoutFrame, pos: Const.addRowButtonPos)
        addSectionButton.frame = buttonRect(view.safeAreaLayoutGuide.layoutFrame, pos: Const.addSectionButtonPos)
    }

    private func addSubviews() {
        view.backgroundColor = .white
        tableView.backgroundColor = .white

        editButton.backgroundColor = Const.editButtonBackgroundColor
        editButton.setTitle(Const.editButtonText, for: .normal)

        addRowButton.backgroundColor = Const.addRowButtonBackgroundColor
        addRowButton.setTitle(Const.addRowButtonText, for: .normal)

        addSectionButton.backgroundColor = Const.addSectionButtonBackgroundColor
        addSectionButton.setTitle(Const.addSectionButtonText, for: .normal)

        view.addSubview(tableView)
        view.addSubview(editButton)
        view.addSubview(addRowButton)
        view.addSubview(addSectionButton)
    }

    private func addButtonTarget() {
        editButton.addTarget(self, action: #selector(editTable), for: .touchUpInside)
        addRowButton.addTarget(self, action: #selector(addRow), for: .touchUpInside)
        addSectionButton.addTarget(self, action: #selector(addSection), for: .touchUpInside)
    }

    @objc func editTable() {
        tableView.isEditing.toggle()
    }

    @objc func addRow() {
        sectionManager.addStudent()
        tableView.beginUpdates()
        tableView.insertRows(at: [IndexPath(row: .zero, section: .zero)], with: .fade)
        tableView.endUpdates()
    }

    @objc func addSection() {
        sectionManager.addSection()
        tableView.beginUpdates()
        tableView.insertSections(IndexSet(integer: 0), with: .fade)
        tableView.endUpdates()
    }

    // MARK: - Data Source
    func numberOfSections(in tableView: UITableView) -> Int {
        return sectionManager.tableSections.count
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard sectionManager.tableSections.indices.contains(section) else { return 0 }
        return sectionManager.tableSections[section].sectionContent.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: .default, reuseIdentifier: Const.cellIdentifier)
        guard
            sectionManager.tableSections.indices.contains(indexPath.section),
            sectionManager.tableSections[indexPath.section].sectionContent.indices.contains(indexPath.row),
            let student = sectionManager.tableSections[indexPath.section].sectionContent[indexPath.row] as? Student
        else { return cell }

        cell.textLabel?.text = student.fullname

        return cell
    }

    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        guard sectionManager.tableSections.indices.contains(section) else { return Const.errorSectionTitle }
        return sectionManager.tableSections[section].sectionName
    }

    func tableView(
        _ tableView: UITableView,
        moveRowAt sourceIndexPath: IndexPath,
        to destinationIndexPath: IndexPath
    ) {
        let sectionToMoveFrom = sectionManager.tableSections[sourceIndexPath.section]
        let sectionToMoveTo = sectionManager.tableSections[destinationIndexPath.section]

        let studentToMove = sectionToMoveFrom.removeElement(at: sourceIndexPath.row)
        sectionToMoveTo.addElement(studentToMove, at: destinationIndexPath.row)
    }

    func tableView(
        _ tableView: UITableView,
        commit editingStyle: UITableViewCell.EditingStyle,
        forRowAt indexPath: IndexPath
    ) {
        guard editingStyle == .delete else { return }

        let sectionToMoveFrom = sectionManager.tableSections[indexPath.section]
        _ = sectionToMoveFrom.removeElement(at: indexPath.row)
        tableView.deleteRows(at: [indexPath], with: .fade)
    }

    // MARK: - Rect creatings
    func tableViewRect(_ frame: CGRect) -> CGRect {
        return CGRect(
            origin: .zero,
            size: CGSize(
                width: frame.width,
                height: frame.height * Const.tableViewHeightOfScreen
            )
        )
    }

    func buttonRect(_ frame: CGRect, pos: Int = 0) -> CGRect {
        return CGRect(
            x: frame.width / Const.buttonCount * CGFloat(pos),
            y: frame.height * Const.tableViewHeightOfScreen,
            width: frame.width / Const.buttonCount,
            height: frame.height * Const.buttonRowHeightOfScreen
        )
    }
}
