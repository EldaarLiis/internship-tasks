//
//  SectionManager.swift
//  Lesson20_01
//
//  Created by Harbros 66 on 17.06.21.
//

import Foundation

private enum Const {
    static let sectionInsertionIndex = 0
    static let studentSegmentInsertionIndex = 0
    static let studentGroupInsertionIndex = 0
    static let numberOfInitialGroups = 5
    static let numberOfStudentsInInitialGroups = 10
}

protocol TableSection {
    var sectionName: String { get }
    var sectionContent: [Any] { get }

    func addElement(_ element: Any, at index: Int)
    func removeElement(at index: Int) -> Any
}

class TableSectionManager {
    var tableSections = [TableSection]()

    init() {
        initialSetup()
    }

    private func initialSetup() {
        (0..<Const.numberOfInitialGroups).forEach { _ in
            tableSections.append(StudentGroup(with: Const.numberOfStudentsInInitialGroups))
        }
        tableSections.sort {
            $0.sectionName < $1.sectionName
        }
    }

    func addSection() {
        tableSections.insert(StudentGroup(), at: Const.sectionInsertionIndex)
    }

    func addStudent(to groupSection: Int = Const.studentSegmentInsertionIndex) {
        guard
            tableSections.indices.contains(groupSection)
        else { return }
        tableSections[groupSection].addElement(Student(), at: Const.studentGroupInsertionIndex)
    }
}
