//
//  Group.swift
//  Lesson20_02
//
//  Created by Harbros 66 on 18.06.21.
//

import Foundation

private enum Const {
    static let groupnames = [
        "Alfa", "Bravo", "Charlie", "Delta", "Echo", "Foxtrot", "Golf", "Hotel", "India",
        "Juliett", "Kilo", "Lima", "Mike", "November", "Oscar", "Papa", "Quebec", "Romeo",
        "Sierra", "Tango", "Uniform", "Victor", "Whiskey", "X-ray", "Yankee", "Zulu"
    ]
}

class StudentGroup: TableSection {
    var sectionName: String
    var sectionContent: [Any]

    init(with numberOfStudents: Int = 0) {
        sectionName = Const.groupnames.randomElement() ?? "Alpha"
        sectionContent = Self.generateStudentGroup(with: numberOfStudents)
    }

    private static func generateStudentGroup(with numberOfStudents: Int) -> [Student] {
        var group = [Student]()

        (0 ..< numberOfStudents).forEach { _ in
            group.append(Student())
        }
        group.sort { $0.fullname < $1.fullname }

        return group
    }

    func addElement(_ element: Any, at index: Int) {
        guard let newStudent = element as? Student else {
            return
        }
        sectionContent.insert(newStudent, at: index)
    }

    func removeElement(at index: Int) -> Any {
        guard let removedStudent = sectionContent.remove(at: index) as? Student else {
            return Student()
        }
        return removedStudent
    }
}
