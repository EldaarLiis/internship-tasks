//
//  NameStruct.swift
//  Lesson20_02
//
//  Created by Harbros 66 on 18.06.21.
//

import Foundation

private enum Const {
    static let names = [
        "james", "robert", "john", "michael", "william", "david", "richard", "joseph", "thomas", "charles",
        "mary", "patricia", "jennifer", "linda", "elizabeth", "barbara", "susan", "jessica", "sarah", "karen"
    ]
    static let lastnames = [
        "smith", "johnson", "williams", "brown", "jones", "miller", "davis", "garcia", "rodriguez", "wilson",
        "martinez", "anderson", "taylor", "thomas", "hernandez", "moore", "martin", "jackson", "thompson", "white"
    ]
}

struct Name {
    var name: String
    var lastname: String

    init() {
        name = Const.names.randomElement()?.capitalized ?? "Ivan"
        lastname = Const.lastnames.randomElement()?.capitalized ?? "Ivanov"
    }

    static func < (lhs: Name, rhs: Name) -> Bool {
        return (lhs.name, lhs.lastname) < (rhs.name, rhs.lastname)
    }
}
