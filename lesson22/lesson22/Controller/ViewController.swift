//
//  ViewController.swift
//  lesson22
//
//  Created by Harbros 66 on 23.06.21.
//

import UIKit

private enum Const {
    static let carouselFrameHeightToMainScreen: CGFloat = 0.2
    static let adFrameHeightToMainScreen: CGFloat = 0.15
    static var mainCatalogueHeightToMainScreen: CGFloat {
        1 - carouselFrameHeightToMainScreen - adFrameHeightToMainScreen
    }
}

class ViewController: UIViewController {
    override var prefersStatusBarHidden: Bool { true }
    lazy var searchBar = UISearchBar(frame: .zero)
    let adMainView = AdMainScrollView()
    let adModel = AdItems()
    let carouselMainView = CarouselMainScrollView()
    let carouselModel = CarouselItems()
    let mainCatalogueMainView = MainCatalogueScrollView()
    let mainCatalogueModel = MainCatalogueItems()

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        navigationItem.titleView = searchBar
        searchBar.delegate = self
        fillStacks()
        addSubviews()
    }

    private func addSubviews() {
        view.addSubview(adMainView)
        view.addSubview(carouselMainView)
        view.addSubview(mainCatalogueMainView)
    }

    private func fillStacks() {
        adMainView.fillStackView(adModel)
        carouselMainView.fillStackView(carouselModel)
        mainCatalogueMainView.initStackView()
    }

    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        carouselMainView.frame = CGRect(
            origin: view.safeAreaLayoutGuide.layoutFrame.origin,
            size: CGSize(
                width: view.safeAreaLayoutGuide.layoutFrame.width,
                height: view.safeAreaLayoutGuide.layoutFrame.height * Const.carouselFrameHeightToMainScreen
            )
        )
        mainCatalogueMainView.frame = CGRect(
            x: view.safeAreaLayoutGuide.layoutFrame.origin.x,
            y: view.safeAreaLayoutGuide.layoutFrame.origin.y +
                view.safeAreaLayoutGuide.layoutFrame.height * Const.carouselFrameHeightToMainScreen,
            width: view.safeAreaLayoutGuide.layoutFrame.width,
            height: view.safeAreaLayoutGuide.layoutFrame.height * Const.mainCatalogueHeightToMainScreen
        )
        mainCatalogueMainView.fillStackView(mainCatalogueModel)
        adMainView.frame = CGRect(
            x: view.safeAreaLayoutGuide.layoutFrame.origin.x,
            y: view.frame.maxY -
                view.safeAreaLayoutGuide.layoutFrame.height * Const.adFrameHeightToMainScreen,
            width: view.safeAreaLayoutGuide.layoutFrame.width,
            height: view.safeAreaLayoutGuide.layoutFrame.height * Const.adFrameHeightToMainScreen
        )
    }
}

extension ViewController: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        guard let searchBarText = searchBar.text else { return }
        mainCatalogueModel.search(by: searchBarText)
        mainCatalogueMainView.fillStackView(mainCatalogueModel)
    }

    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        searchBar.setShowsCancelButton(true, animated: true)
    }

    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        searchBar.setShowsCancelButton(false, animated: true)
    }

    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.text = .none
        mainCatalogueModel.search()
        searchBar.endEditing(true)
        mainCatalogueMainView.fillStackView(mainCatalogueModel)
    }
}
