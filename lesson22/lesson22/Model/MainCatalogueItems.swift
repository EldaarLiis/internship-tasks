//
//  MainCatalogueItems.swift
//  Lesson21
//
//  Created by Harbros 66 on 21.06.21.
//

import UIKit

private enum Const {
    static let cellCornerRadius: CGFloat = 5

    static let emptySearchString = " "
}

class MainCatalogueItems {
    private var initialCardDeque = [Card]()
    private(set) var cardDeque = [Card]()
    var itemQ: Int { cardDeque.count }

    init() {
        fillDeque()
        search()
    }

    private func fillDeque() {
        Suit.allCases.forEach { suit in
            Worth.allCases.forEach { worth in
                let card = Card(suit: suit, worth: worth)
                initialCardDeque.append(card)
            }
        }
        initialCardDeque.shuffle()
    }

    func search(by searchString: String = Const.emptySearchString) {
        guard !searchString.isEmpty else {
            cardDeque = initialCardDeque
            return
        }
        cardDeque = initialCardDeque.filter {
            $0.cardLabel.localizedCaseInsensitiveContains(searchString)
        }
    }
}
