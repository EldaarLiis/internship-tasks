//
//  ConvenienceFuncs.swift
//  Lesson21
//
//  Created by Harbros 66 on 23.06.21.
//

import UIKit

private enum Const {
    static let carouselFrameHeightToMainScreen: CGFloat = 0.2
    static let adFrameHeightToMainScreen: CGFloat = 0.15
    static var mainCatalogueHeightToMainScreen: CGFloat {
        1 - carouselFrameHeightToMainScreen - adFrameHeightToMainScreen
    }

    static let preferredMainCatalogueWidth: CGFloat = 100
    static let preferredMainCatalogueHeight: CGFloat = 110

    static var widthSizeOffset: CGFloat = 4
    static var heightSizeOffset: CGFloat = 4

    static let adViewItemDivisionRate: CGFloat = 0.5
}

class ConvenienceFuncs {
    static func mainCatalogueItemViewSize(_ mainCatalogueFrame: CGRect) -> CGSize {
        return CGSize(
            width: mainCatalogueFrame.width /
                floor(mainCatalogueFrame.width / Const.preferredMainCatalogueWidth),
            height: mainCatalogueFrame.height /
                floor(mainCatalogueFrame.height / Const.preferredMainCatalogueHeight)
        )
    }

    static func findItemSize(mainFrameSize: CGSize, preferredItemSize: CGSize) -> CGSize {
        return CGSize(
            width: mainFrameSize.width /
                floor(mainFrameSize.width / preferredItemSize.width),
            height: mainFrameSize.height /
                floor(mainFrameSize.height / preferredItemSize.height)
        )
    }

    static func findNumberOfRowsAndColumns(
        mainFrameSize: CGSize,
        itemSize: CGSize,
        numberOfItems: Int
    ) -> (rows: Int, columns: Int) {
        let columns = mainFrameSize.width / itemSize.width
        let rows = Int(ceil(CGFloat(numberOfItems) / columns))
        return (rows, Int(columns))
    }
}
