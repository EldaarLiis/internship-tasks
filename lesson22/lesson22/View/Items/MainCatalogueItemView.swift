//
//  MainCatalogueCollectionViewCell.swift
//  Lesson21
//
//  Created by Harbros 66 on 22.06.21.
//

import UIKit

private enum Const {
    static var verticalSizeOffset: CGFloat = 4
    static var horizontalSizeOffset: CGFloat = 4

    static var labelHeight: CGFloat = 15
    static var fontSize: CGFloat = 12

    static var half: CGFloat = 0.5
    static let cellCornerRadius: CGFloat = 5
}

class MainCatalogueItemView: UIView {
    private let label = UILabel()
    private let cardIconImageView = UIImageView()
    private let card: Card
    init(size: CGSize, card: Card) {
        self.card = card
        super.init(frame: CGRect(origin: .zero, size: size))

        label.text = card.cardLabel
        label.font = label.font.withSize(Const.fontSize)
        label.textAlignment = .center

        guard let image = UIImage(named: card.imagename) else { return }
        cardIconImageView.image = image

        addSubview(label)
        addSubview(cardIconImageView)

        self.layer.cornerRadius = Const.cellCornerRadius
        self.clipsToBounds = true
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        // labels:
        label.frame = CGRect(
            x: bounds.minX,
            y: bounds.maxY - Const.labelHeight,
            width: bounds.width,
            height: Const.labelHeight
        )

        // card image:
        let iconFrameSide = min(
            (bounds.width - Const.horizontalSizeOffset) * Const.half,
            (bounds.height - Const.verticalSizeOffset - Const.labelHeight) * Const.half
        )
        cardIconImageView.frame.size = CGSize(width: iconFrameSide, height: iconFrameSide)
        cardIconImageView.center = CGPoint(
            x: bounds.midX,
            y: bounds.midY - Const.labelHeight * Const.half
        )
    }

    required init?(coder: NSCoder) {
        card = Card(suit: .hearts, worth: .ace)
        super.init(coder: coder)
    }
}
