//
//  MainCatalogueRowView.swift
//  lesson22
//
//  Created by Harbros 66 on 24.06.21.
//

import UIKit

class MainCatalogueRowView: UIStackView {
    func fillRowStackView(_ rowItems: [UIView]) {
        rowItems.forEach { cardView in
            addArrangedSubview(cardView)
            frame.size.width += cardView.frame.width
            frame.size.height = cardView.frame.height
        }
        self.distribution = .fillEqually
        self.axis = .horizontal
    }
}
