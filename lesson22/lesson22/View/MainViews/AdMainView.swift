//
//  AdMainView.swift
//  lesson22
//
//  Created by Harbros 66 on 23.06.21.
//

import UIKit
private enum Const {
    static let half: CGFloat = 0.5
}

class AdMainScrollView: UIScrollView {
    private var adStackView = UIStackView()

    func fillStackView(_ adModel: AdItems) {
        let imageFrame = CGRect(
            origin: .zero,
            size: CGSize(
                width: bounds.width * Const.half,
                height: bounds.height
            )
        )
        adModel.adItemPool.forEach { adItem in
            let imageView = AdItemImageView(frame: imageFrame, image: adItem)
            adStackView.addArrangedSubview(imageView)
        }
        adStackView.distribution = .fillEqually
        addSubview(adStackView)
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        adStackView.frame = bounds
    }
}
