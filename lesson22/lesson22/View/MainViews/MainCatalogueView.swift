//
//  MainCatalogueView.swift
//  lesson22
//
//  Created by Harbros 66 on 23.06.21.
//

import UIKit

private enum Const {
    static let preferredMainCatalogueWidth: CGFloat = 100
    static let preferredMainCatalogueHeight: CGFloat = 110

    static var preferredItemSize: CGSize {
        return CGSize(
            width: preferredMainCatalogueWidth,
            height: preferredMainCatalogueHeight
        )
    }
}

class MainCatalogueScrollView: UIScrollView {
    private var mainCatalogueStackView = UIStackView()

    func initStackView() {
        mainCatalogueStackView.axis = .vertical
        mainCatalogueStackView.distribution = .fillEqually
        addSubview(mainCatalogueStackView)
    }

    func fillStackView(_ mainCatalogueModel: MainCatalogueItems) {
        mainCatalogueStackView.removeAllArrangedSubviews()
        let itemSize = ConvenienceFuncs.findItemSize(
            mainFrameSize: frame.size,
            preferredItemSize: Const.preferredItemSize
        )
        let itemCount = ConvenienceFuncs.findNumberOfRowsAndColumns(
            mainFrameSize: frame.size,
            itemSize: itemSize,
            numberOfItems: mainCatalogueModel.itemQ
        )

        (0..<itemCount.rows).forEach { row in
            let lowerBound = row * itemCount.columns
            let upperBound = min((row + 1) * itemCount.columns, mainCatalogueModel.itemQ)

            let currentCardRow = mainCatalogueModel.cardDeque[lowerBound ..< upperBound]
            let currentCardViewRow = currentCardRow.map { card in
                MainCatalogueItemView(size: itemSize, card: card)
            }
            let currentRowStackView = MainCatalogueRowView()
            currentRowStackView.fillRowStackView(currentCardViewRow)
            currentRowStackView.frame.size = CGSize(
                width: frame.width,
                height: itemSize.height
            )
            mainCatalogueStackView.addArrangedSubview(currentRowStackView)
        }

        mainCatalogueStackView.frame.size = CGSize(
            width: frame.width,
            height: itemSize.height * CGFloat(itemCount.rows)
        )
        self.contentSize = mainCatalogueStackView.frame.size
    }
}
