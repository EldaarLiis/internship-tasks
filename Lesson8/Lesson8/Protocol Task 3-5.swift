//
//  task4.swift
//  Lesson8
//
//  Created by Harbros 66 on 20.05.21.
//

import Foundation

protocol Storable {
    var expired: Bool { get }
    var daysToExpire: Int { get }
}

class StorableFood: Food, Storable {
    let name: String
    let whatDoesItTasteLike: String
    var expired: Bool
    var daysToExpire: Int

    init(name: String, whatDoesItTasteLike: String, expired: Bool, daysToExpire: Int) {
        if (expired && daysToExpire != 0) || (!expired && daysToExpire == 0) {
            print("WTH. This item is expired, but input is wrong I'm fixing that for you.")
            self.daysToExpire = 0
            self.expired = true
        } else {
            self.daysToExpire = daysToExpire
            self.expired = expired
        }
        self.name = name
        self.whatDoesItTasteLike = whatDoesItTasteLike
    }

    func taste() {
        print("\(name.lowercased()) tastes \(whatDoesItTasteLike.lowercased())")
    }
    // task three initializer
    /*
     init(name: String, whatDoesItTasteLike: String, expired: Bool) {
     self.expired = expired
     super.init(name: name, whatDoesItTasteLike: whatDoesItTasteLike)
     }
     */
}

class Fridge {
    var foodInFridge = [StorableFood]()

    func addToFridge(storableItem: StorableFood) {
        if storableItem.expired {
            print("Ew... \(storableItem.name) stinks. Throw it away!")
        } else {
            foodInFridge.append(storableItem)
            print("\(storableItem.name) has been moved to fridge")
        }
    }

    func addToFridge(storableItemArray: [StorableFood]) {
        storableItemArray.forEach {
            addToFridge(storableItem: $0)
        }
    }

    func whatsInFridge() {
        foodInFridge.forEach {
            print("\($0.name) is in fridge. It has \($0.daysToExpire) days to be eaten.")
        }
    }

    func sortFridge() {
        foodInFridge.sort {
            ($1.name, $1.daysToExpire) > ($0.name, $0.daysToExpire)
        }
    }
}

extension Lesson8 {
    func taskFour() {
        // task three items initialization
        /*
         let potato = NonstorableItem(name: "Potato", whatDoesItTasteLike: "Plain")
         let strawberry = StorableItem(name: "Strawberry", whatDoesItTasteLike: "Sweet", expired: false)
         let milk = StorableItem(name: "Milk", whatDoesItTasteLike: "milky :)", expired: true)
         */
        let strawberry = StorableFood(name: "Strawberry", whatDoesItTasteLike: "Sweet", expired: false, daysToExpire: 3)
        let milk = StorableFood(name: "Milk", whatDoesItTasteLike: "Milky :)", expired: true, daysToExpire: 0)
        let meat = StorableFood(name: "Meat", whatDoesItTasteLike: "Chewy", expired: false, daysToExpire: 1)
        let sauce = StorableFood(name: "Cheese sauce", whatDoesItTasteLike: "Cheesy", expired: false, daysToExpire: 15)

        let storableItemsOnTheTable = [strawberry, milk, meat, sauce]

        let fridge = Fridge()
        storableItemsOnTheTable.forEach {
            fridge.addToFridge(storableItem: $0)
        }
        fridge.sortFridge()
        fridge.whatsInFridge()
    }
}
