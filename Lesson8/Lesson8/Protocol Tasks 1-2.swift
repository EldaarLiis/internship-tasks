//
//  task3.swift
//  Lesson8
//
//  Created by Harbros 66 on 20.05.21.
//

import Foundation

// Task Three:
protocol Food {
    var name: String { get }
    func taste()
}

// class definition for task 3
class NonstorableFood: Food {
    let name: String
    let whatDoesItTasteLike: String
    init(name: String, whatDoesItTasteLike: String) {
        self.name = name
        self.whatDoesItTasteLike = whatDoesItTasteLike
    }
    func taste() {
        print("\(name.lowercased()) tastes \(whatDoesItTasteLike.lowercased())")
    }
}

// Task Three Functions. I liked it like this.
/*
extension Array where Element == NonstorableItem {
    mutating func sortPouch() {
        self.sort {
            $1.name > $0.name
        }
    }
    func tastePouch() {
        self.forEach {
            $0.taste()
        }
    }
}
 */
// But one guy advised not to do it like that.
// Because it's bad.
// So i decided to make a class for pouch with such methods.
class Pouch {
    var pouch = [NonstorableFood]()
    func addItems(itemsToAdd: [NonstorableFood]) {
        pouch.append(contentsOf: itemsToAdd)
    }
    func addItem(itemToAdd: NonstorableFood) {
        pouch.append(itemToAdd)
    }
    func sort() {
        pouch.sort { $1.name > $0.name }
    }
    func tastePouch() {
        pouch.forEach { $0.taste() }
    }
}

class Lesson8 {
    func taskThree() {
        // initialization of food items
        let potato = NonstorableFood(name: "Potato", whatDoesItTasteLike: "Plain")
        let strawberry = NonstorableFood(name: "Strawberry", whatDoesItTasteLike: "Sweet")
        let milk = NonstorableFood(name: "Milk", whatDoesItTasteLike: "milky :)")

        let pouch = Pouch()
        pouch.addItems(itemsToAdd: [potato, strawberry, milk])
        // i made tasks that required a function that returns something as methods of pouch class
        // i'll make separate functions if you say so
        pouch.sort()
        pouch.tastePouch()
    }
}
