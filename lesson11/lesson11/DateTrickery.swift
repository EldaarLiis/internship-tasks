//
//  Date trickery.swift
//  lesson11
//
//  Created by Harbros 66 on 27.05.21.
//

import Foundation

class DateTrickery {
    func generateBirthDay(youngestAge: Int = 16, oldestAge: Int = 50) -> Date {
        guard
            let startingDate = generateDateNYearsAgo(yearsAgo: oldestAge),
            let endingDate = generateDateNYearsAgo(yearsAgo: youngestAge),
            startingDate < endingDate
        else { return Date() }
        let randomTimeSpan = TimeInterval.random(
            in: startingDate.timeIntervalSinceNow..<endingDate.timeIntervalSinceNow
        )
        return Date(timeIntervalSinceNow: randomTimeSpan)
    }

    func generateDateNYearsAgo(yearsAgo: Int, fromDate: Date = Date()) -> Date? {
        let fromDateComponents = Calendar.current.dateComponents([.day, .month, .year], from: fromDate)
        guard let passedYear = fromDateComponents.year else { return nil }
        let dateNYearsAgo = DateComponents(
            year: passedYear - yearsAgo,
            month: fromDateComponents.month,
            day: fromDateComponents.day
        )
        return Calendar.current.date(from: dateNYearsAgo)
    }

    static func getNextDay(date: Date?) -> Date {
        guard
            let unwrappedDate = date,
            let nextDay = Calendar.current.date(byAdding: DateComponents(day: 1), to: unwrappedDate)
        else {
            return Date()
        }
        return nextDay
    }

    static func printDate(date: Date?) -> String {
        guard let unwrappedDate = date else { return "" }
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd.MM.yyyy"
        return dateFormatter.string(from: unwrappedDate)
    }

    static func areDatesEqual(lhs: Date, rhs: Date) -> Bool {
        return  Calendar.current.isDate(lhs, equalTo: rhs, toGranularity: .day) &&
                Calendar.current.isDate(lhs, equalTo: rhs, toGranularity: .month) &&
                Calendar.current.isDate(lhs, equalTo: rhs, toGranularity: .year)
    }
}
