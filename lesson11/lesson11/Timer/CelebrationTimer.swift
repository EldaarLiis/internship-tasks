//
//  Celebration Timer.swift
//  lesson11
//
//  Created by Harbros 66 on 27.05.21.
//

import UIKit

class CelebrityTimer {
    var timer: Timer?
    var studentGroup = StudentGroup()
    var dateToCheck: Date?
    let dateGetter = DateTrickery()

    func taskTest() {
        studentGroup.fillGroupWithNStudents()
        dateToCheck = dateGetter.generateDateNYearsAgo(yearsAgo: 50)

        timer = Timer.scheduledTimer(
            timeInterval: 0.001,
            target: self,
            selector: #selector(checkDay),
            userInfo: nil,
            repeats: true
        )
    }

    @objc func checkDay() {
        guard let date = dateToCheck else { return }
        studentGroup.whosBirthdayIsToday(dateToCheck: date)
        if
            DateTrickery.areDatesEqual(lhs: date, rhs: Date())
        {
            print("Time's up!")
            timer?.invalidate()
        }
        dateToCheck = DateTrickery.getNextDay(date: dateToCheck)
    }
}
