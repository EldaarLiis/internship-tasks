//
//  ViewController.swift
//  lesson11
//
//  Created by Harbros 66 on 26.05.21.
//

import UIKit

class ViewController: UIViewController {
    let l11DateTask = Lesson11DateTask()
    let l11CelebrityTimer = CelebrityTimer()
    let l11DateCounting = DateCounting()

    override func viewDidLoad() {
        super.viewDidLoad()
        // l11DateTask.taskOne()
        // l11CelebrityTimer.taskTest()
        l11DateCounting.task()
    }
}
