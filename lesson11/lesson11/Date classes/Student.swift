//
//  Student.swift
//  lesson11
//
//  Created by Harbros 66 on 26.05.21.
//

import Foundation

let firstNames = ["James", "Robert", "John", "Michael", "William", "Mary", "Patricia", "Jennifer", "Linda", "Elizabeth"]
let lastNames = ["Smith", "Johnson", "Williams", "Brown", "Jones", "Miller", "Davis", "Garcia", "Wilson", "Martinez"]

class Student {
    private let birthdayGenerator = DateTrickery()
    private let dateOfBirth: Date
    internal let firstName: String
    private let lastName: String

    init() {
        dateOfBirth = birthdayGenerator.generateBirthDay()
        firstName = firstNames.randomElement() ?? "Ivan"
        lastName = lastNames.randomElement() ?? "Ivanov"
    }

    func birthDayString() -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd.MM.yyyy"
        return dateFormatter.string(from: dateOfBirth)
    }

    func printPersonalInfo() {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd.MM.yyyy"
        print(
            "Name: \(firstName), Surname: \(lastName).",
            "Birth date: \(birthDayString())"
        )
    }

    // MARK: - Sorting types
    static func < (lhs: Student, rhs: Student) -> Bool {
        return lhs.dateOfBirth < rhs.dateOfBirth
    }

    static func > (lhs: Student, rhs: Student) -> Bool {
        return lhs.dateOfBirth > rhs.dateOfBirth
    }

    static func << (lhs: Student, rhs: Student) -> Bool {
        return (lhs.firstName, lhs.lastName, lhs.dateOfBirth) < (rhs.firstName, rhs.lastName, rhs.dateOfBirth)
    }

    static func >> (lhs: Student, rhs: Student) -> Bool {
        return (lhs.firstName, lhs.lastName, lhs.dateOfBirth) > (rhs.firstName, rhs.lastName, rhs.dateOfBirth)
    }

    // MARK: - Date comparison
    static func == (lhs: Student, rhs: Date) -> Bool {
        return DateTrickery.areDatesEqual(lhs: lhs.dateOfBirth, rhs: rhs)
    }
}
