//
//  StudentGroup.swift
//  lesson11
//
//  Created by Harbros 66 on 28.05.21.
//

import Foundation

class StudentGroup {
    var studentGroup = [Student]()

    func fillGroupWithNStudents(number: Int = 10) {
        (1...number).forEach { _ in
            studentGroup.append(Student())
        }
    }

    func addStudent(student: Student) {
        studentGroup.append(student)
    }

    func printGroupPersonalInfo() {
        studentGroup.forEach { student in
            student.printPersonalInfo()
        }
    }

    // MARK: - transition for sort and max
    func max(byOp: (Student, Student) -> Bool) -> Student? {
        return studentGroup.max(by: byOp)
    }

    func sort(byOp: (Student, Student) -> Bool) {
        studentGroup.sort(by: byOp)
    }
}
