//
//  ViewController.swift
//  lesson15
//
//  Created by Harbros 66 on 31.05.21.
//

import UIKit

class ViewController: UIViewController {
    private var chess = ChessBoard()
    private var anime = Animation()

    override func viewDidLoad() {
        super.viewDidLoad()

        addSubviews()
    }

    private func addSubviews() {
        view.addSubview(chess.mainView)
//        view.addSubview(anime.mainView)
    }

    override var prefersStatusBarHidden: Bool {
        return true
    }

    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        chess.rotateBoard(isLandscape: UIDevice.current.orientation.isLandscape)
    }
}
