//
//  MainFrame.swift
//  Lesson21
//
//  Created by Harbros 66 on 28.06.21.
//

import UIKit

private enum Const {
    static let carouselFrameHeightToMainScreen: CGFloat = 0.2
    static let adFrameHeightToMainScreen: CGFloat = 0.15
    static var mainCatalogueHeightToMainScreen: CGFloat {
        1 - carouselFrameHeightToMainScreen - adFrameHeightToMainScreen
    }
}

class MainView: UIView {
    private(set) var carouselCollectionView: UICollectionView
    private(set) var mainCatalogueCollectionView: UICollectionView
    private(set) var adCollectionView: UICollectionView

    convenience init() {
        self.init(frame: UIScreen.main.bounds)
    }

    override init(frame: CGRect) {
        // carousel init
        carouselCollectionView = UICollectionView(
            frame: .zero,
            collectionViewLayout: UICollectionViewLayout()
        )

        // main catalogue init
        mainCatalogueCollectionView = UICollectionView(
            frame: .zero,
            collectionViewLayout: UICollectionViewLayout()
        )

        // ad init
        adCollectionView = UICollectionView(
            frame: .zero,
            collectionViewLayout: UICollectionViewLayout()
        )
        super.init(frame: frame)
        registerCollectionViews()
        applyCollectionViewSettings()
    }

    override func layoutSubviews() {
        let carouselFrame = CGRect(
            x: bounds.minX,
            y: bounds.minY,
            width: bounds.width,
            height: bounds.height * Const.carouselFrameHeightToMainScreen
        )
        let mainCatalogueFrame = CGRect(
            x: bounds.origin.x,
            y: carouselFrame.maxY,
            width: bounds.width,
            height: bounds.height * Const.mainCatalogueHeightToMainScreen
        )
        let adFrame = CGRect(
            x: bounds.origin.x,
            y: mainCatalogueFrame.maxY,
            width: bounds.width,
            height: bounds.maxY - mainCatalogueFrame.maxY
        )

        carouselCollectionView.frame = carouselFrame
        let carouselCollectionViewLayout = ConvenienceFuncs.carouselLayout(carouselFrame)
        carouselCollectionView.setCollectionViewLayout(carouselCollectionViewLayout, animated: false)

        mainCatalogueCollectionView.frame = mainCatalogueFrame
        let mainCatalogueCollectionViewLayout = ConvenienceFuncs.mainCatalogueLayout(mainCatalogueFrame)
        mainCatalogueCollectionView.setCollectionViewLayout(mainCatalogueCollectionViewLayout, animated: false)

        adCollectionView.frame = adFrame
        let adCollectionViewLayout = ConvenienceFuncs.adLayout(adFrame)
        adCollectionView.setCollectionViewLayout(adCollectionViewLayout, animated: false)
    }

    private func registerCollectionViews() {
        carouselCollectionView.register(
            CarouselCollectionViewCell.self,
            forCellWithReuseIdentifier: CellID.carouselCellID
        )
        mainCatalogueCollectionView.register(
            MainCatalogueCollectionViewCell.self,
            forCellWithReuseIdentifier: CellID.mainCellID
        )
        adCollectionView.register(
            AdCollectionViewCell.self,
            forCellWithReuseIdentifier: CellID.adCellID
        )

        carouselCollectionView.backgroundColor = .white
        mainCatalogueCollectionView.backgroundColor = .white
        adCollectionView.backgroundColor = .white
        addSubview(carouselCollectionView)
        addSubview(mainCatalogueCollectionView)
        addSubview(adCollectionView)
    }

    private func applyCollectionViewSettings() {
        carouselCollectionView.isPagingEnabled = true
        carouselCollectionView.alwaysBounceHorizontal = true
        carouselCollectionView.showsHorizontalScrollIndicator = false

        carouselCollectionView.showsVerticalScrollIndicator = false

        adCollectionView.isScrollEnabled = false
        adCollectionView.showsHorizontalScrollIndicator = false
    }

    required init?(coder: NSCoder) { fatalError("init(coder:) has not been implemented") }
}
