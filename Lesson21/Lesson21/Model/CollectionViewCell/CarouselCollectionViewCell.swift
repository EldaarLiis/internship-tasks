//
//  CarouselCollectionViewCell.swift
//  Lesson21
//
//  Created by Harbros 66 on 22.06.21.
//

import UIKit

private enum Const {
    static let carouselCellCornerRadius: CGFloat = 5
}

class CarouselCollectionViewCell: UICollectionViewCell {
    private let imageView = UIImageView()

    func setupCell(image: UIImage) {
        imageView.frame = self.contentView.frame
        imageView.image = image
        contentView.addSubview(imageView)

        self.layer.cornerRadius = Const.carouselCellCornerRadius
        self.clipsToBounds = true
    }

    override func layoutSubviews() {
        imageView.frame = contentView.frame
    }
}
