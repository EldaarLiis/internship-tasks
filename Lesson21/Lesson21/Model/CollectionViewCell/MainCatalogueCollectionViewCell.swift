//
//  MainCatalogueCollectionViewCell.swift
//  Lesson21
//
//  Created by Harbros 66 on 22.06.21.
//

import UIKit

private enum Const {
    static var verticalSizeOffset: CGFloat = 4
    static var horizontalSizeOffset: CGFloat = 4

    static var labelHeight: CGFloat = 15
    static var fontSize: CGFloat = 12

    static var half: CGFloat = 0.5
    static let cardLabelMiddleterm = " of "

    static let cellCornerRadius: CGFloat = 5
}

class MainCatalogueCollectionViewCell: UICollectionViewCell {
    private let label = UILabel()
    private let cardIconImageView = UIImageView()

    func setupCell(card: Card) {
        resizeAndCenterElements()
        // labels:
        label.text = card.cardLabel
        label.font = label.font.withSize(Const.fontSize)
        label.textAlignment = .center

        // card image:
        guard let image = UIImage(named: card.imagename) else { return }
        cardIconImageView.image = image

        contentView.addSubview(label)
        contentView.addSubview(cardIconImageView)

        self.layer.cornerRadius = Const.cellCornerRadius
        self.clipsToBounds = true
    }

    private func resizeAndCenterElements() {
        label.frame = CGRect(
            x: contentView.frame.minX,
            y: contentView.frame.height - Const.labelHeight,
            width: contentView.frame.width,
            height: Const.labelHeight
        )

        let iconFrameSide = min(
            (contentView.frame.width - Const.horizontalSizeOffset) * Const.half,
            (contentView.frame.height - Const.verticalSizeOffset - Const.labelHeight) * Const.half
        )
        cardIconImageView.frame.size = CGSize(width: iconFrameSide, height: iconFrameSide)
        cardIconImageView.center = CGPoint(
            x: contentView.frame.midX,
            y: contentView.frame.midY - Const.labelHeight * Const.half
        )
    }
}
