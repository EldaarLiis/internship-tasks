//
//  Card.swift
//  Lesson21
//
//  Created by Harbros 66 on 21.06.21.
//

import UIKit

private enum Const {
    static let imageNamePrefix = "card-"
    static let hyphen = "-"
    static let cardLabelMiddleterm = " of "
}

enum Suit: String, CaseIterable {
    case clubs
    case diamonds
    case hearts
    case spades
}

enum Worth: String, CaseIterable {
    case two = "2"
    case three = "3"
    case four = "4"
    case five = "5"
    case six = "6"
    case seven = "7"
    case eight = "8"
    case nine = "9"
    case ten = "10"
    case jack = "jack"
    case queen = "queen"
    case king = "king"
    case ace = "ace"
}

struct Card {
    let suit: Suit
    let worth: Worth
    var cardLabel: String {
        return worth.rawValue.capitalized + Const.cardLabelMiddleterm + suit.rawValue.capitalized
    }
    private(set) var image: UIImage?

    init(suit: Suit, worth: Worth) {
        self.suit = suit
        self.worth = worth
        image = UIImage(named: self.imagename)
    }

    var imagename: String {
        Const.imageNamePrefix + self.worth.rawValue + Const.hyphen + self.suit.rawValue
    }
}
