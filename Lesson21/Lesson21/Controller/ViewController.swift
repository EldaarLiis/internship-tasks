//
//  ViewController.swift
//  Lesson21
//
//  Created by Harbros 66 on 19.06.21.
//

import UIKit

private enum Const {
    static let carouselFrameHeightToMainScreen: CGFloat = 0.2
    static let adFrameHeightToMainScreen: CGFloat = 0.15
    static var mainCatalogueHeightToMainScreen: CGFloat {
        1 - carouselFrameHeightToMainScreen - adFrameHeightToMainScreen
    }
}

enum CellID {
    static let carouselCellID = "carouselCell"
    static let mainCellID = "mainCell"
    static let adCellID = "adCell"
}

class ViewController: UIViewController {
    override var prefersStatusBarHidden: Bool { true }
    lazy var searchBar = UISearchBar(frame: .zero)
    private(set) var mainView = MainView()
    private(set) var carouselModel = CarouselItems()
    private(set) var catalogueModel = MainCatalogueItems()
    private(set) var adModel = AdItems()

    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.titleView = searchBar
        searchBar.delegate = self
        view.addSubview(mainView)

        mainView.adCollectionView.dataSource = self
        mainView.carouselCollectionView.dataSource = self
        mainView.mainCatalogueCollectionView.dataSource = self
    }

    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        view.backgroundColor = .white
        mainView.frame = view.safeAreaLayoutGuide.layoutFrame
    }

    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        searchBar.endEditing(true)
    }
}

extension ViewController: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        guard let searchBarText = searchBar.text else { return }
        catalogueModel.search(by: searchBarText)
        mainView.mainCatalogueCollectionView.reloadData()
    }

    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        searchBar.setShowsCancelButton(true, animated: true)
    }

    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        searchBar.setShowsCancelButton(false, animated: true)
    }

    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.text = .none
        catalogueModel.search()
        searchBar.endEditing(true)
        mainView.mainCatalogueCollectionView.reloadData()
    }
}
