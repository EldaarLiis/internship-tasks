//
//  CollectionViewDataSourceExt.swift
//  Lesson21
//
//  Created by Harbros 66 on 22.06.21.
//

import UIKit

extension ViewController: UICollectionViewDelegate, UICollectionViewDataSource {

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        switch collectionView {
        case mainView.mainCatalogueCollectionView:
            return catalogueModel.cardDeque.count
        case mainView.adCollectionView:
            return adModel.adItemPool.count
        default:
            return carouselModel.carouselItemPool.count
        }
    }

    func collectionView(
        _ collectionView: UICollectionView,
        cellForItemAt indexPath: IndexPath
    ) -> UICollectionViewCell {
        switch collectionView {
        case mainView.mainCatalogueCollectionView:
            let cell = mainView.mainCatalogueCollectionView.dequeueReusableCell(
                withReuseIdentifier: CellID.mainCellID,
                for: indexPath
            ) as? MainCatalogueCollectionViewCell
            guard
                let itemCollectionViewCell = cell,
                catalogueModel.cardDeque.indices.contains(indexPath.row)
            else { return UICollectionViewCell() }
            itemCollectionViewCell.setupCell(card: catalogueModel.cardDeque[indexPath.row])
            return itemCollectionViewCell
        case mainView.adCollectionView:
            let cell = mainView.adCollectionView.dequeueReusableCell(
                withReuseIdentifier: CellID.adCellID,
                for: indexPath
            ) as? AdCollectionViewCell
            guard
                let itemCollectionViewCell = cell,
                adModel.adItemPool.indices.contains(indexPath.item)
            else { return UICollectionViewCell() }
            itemCollectionViewCell.setupCell(image: adModel.adItemPool[indexPath.item])
            return itemCollectionViewCell
        default:
            let cell = mainView.carouselCollectionView.dequeueReusableCell(
                withReuseIdentifier: CellID.carouselCellID,
                for: indexPath
            ) as? CarouselCollectionViewCell
            guard
                let itemCollectionViewCell = cell,
                carouselModel.carouselItemPool.indices.contains(indexPath.item)
            else { return UICollectionViewCell() }
            itemCollectionViewCell.setupCell(image: carouselModel.carouselItemPool[indexPath.item])
            return itemCollectionViewCell
        }
    }
}
