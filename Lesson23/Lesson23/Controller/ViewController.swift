//
//  ViewController.swift
//  Lesson23
//
//  Created by Harbros 66 on 25.06.21.
//

import UIKit

private enum Const {
    static let cellID = "tableCellID"

    static let cellSpacing: CGFloat = 5
}

class ViewController: UIViewController {
    override var prefersStatusBarHidden: Bool { true }
    let messageTable = UITableView()
    let messageModel = MessageTray()

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white

        styleTable()
        messageTable.dataSource = self
        messageTable.delegate = self
        messageTable.register(MessageTableCell.self, forCellReuseIdentifier: Const.cellID)
        view.addSubview(messageTable)
    }

    private func styleTable() {
        messageTable.backgroundColor = .white

        messageTable.separatorStyle = .none
        messageTable.showsVerticalScrollIndicator = false
    }

    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        messageTable.frame = view.safeAreaLayoutGuide.layoutFrame
    }
}

extension ViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return messageModel.messageArray.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = messageTable.dequeueReusableCell(
            withIdentifier: Const.cellID,
            for: indexPath
        ) as? MessageTableCell
        let optMessage = messageModel.messageArray.safe(at: indexPath.row)
        guard
            let messageCell = cell,
            let message = optMessage
        else {
            return UITableViewCell()
        }
        messageCell.setupCell(with: message)
        return messageCell
    }
}

extension ViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let messageAtRow = messageModel.messageArray.safe(at: indexPath.item)
        guard let height = messageAtRow?.frame.height else { return .zero }
        return height + Const.cellSpacing
    }
}
