//
//  MessageView.swift
//  Lesson23
//
//  Created by Harbros 66 on 25.06.21.
//

import UIKit

private enum Const {
    static let maxWidth = UIScreen.main.bounds.width * 0.7

    static let labelFontSize: CGFloat = 20
    static let bubbleRoundRadius: CGFloat = 20
    static let bubbleTailHeight: CGFloat = 30
    static let bubbleTailWidth: CGFloat = 12
    static let bubbleTailLowWidth: CGFloat = 24
    static let bubbleTailLowHeight: CGFloat = 8
    static let two: CGFloat = 2
    static let zeroDegreeAngle: CGFloat = .zero
    static let ninetyDegreeAngle: CGFloat = .pi / 2
    static let oneEightyDegreeAngle: CGFloat = .pi
    static let twoSeventyDegreeAngle: CGFloat = .pi * 3 / 2

    static var widthAdditional: CGFloat {
        2 * bubbleRoundRadius + bubbleTailWidth
    }
    static var heightAdditional: CGFloat {
        2 * bubbleRoundRadius
    }
}

private enum Style {
    static let incomeMessageBubbleFillColor: CGColor = UIColor.lightGray.cgColor
    static let outcomeMessageBubbleFillColor: CGColor = UIColor.systemBlue.cgColor

    static let messageBubbleStrokeColor: CGColor = UIColor.black.cgColor
}

class MessageView: UIView {
    private let messageLabel = UILabel()
    private let bubbleLayer = CAShapeLayer()
    private(set) var isIncoming = true

    init() {
        super.init(frame: .zero)
        create(with: Message())
    }

    func create(with message: Message) {
        self.isIncoming = message.isIncoming
        messageLabel.font = messageLabel.font.withSize(Const.labelFontSize)
        let messageLabelFrameSize = message.labelSize(font: messageLabel.font, maxWidth: Const.maxWidth)
        frame.size = CGSize(
            width: messageLabelFrameSize.width + Const.widthAdditional,
            height: messageLabelFrameSize.height + Const.heightAdditional
        )

        messageLabel.text = message.text
        messageLabel.numberOfLines = .zero
        messageLabel.lineBreakMode = .byWordWrapping
        messageLabel.contentMode = .scaleToFill
        addSubview(messageLabel)

        bubbleLayer.lineWidth = .zero
        bubbleLayer.strokeColor = Style.messageBubbleStrokeColor

        bubbleLayer.path = message.isIncoming
            ? createIncomeMessageBubble()
            : createOutcomeMessageBubble()
        bubbleLayer.fillColor = message.isIncoming
            ? Style.incomeMessageBubbleFillColor
            : Style.outcomeMessageBubbleFillColor
        layer.insertSublayer(bubbleLayer, at: 0)

        let labelXOffset = message.isIncoming
            ? Const.bubbleTailWidth / Const.two
            : -Const.bubbleTailWidth / Const.two

        messageLabel.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            messageLabel.centerYAnchor.constraint(equalTo: self.centerYAnchor),
            messageLabel.centerXAnchor.constraint(equalTo: self.centerXAnchor, constant: labelXOffset),
            messageLabel.widthAnchor.constraint(equalToConstant: messageLabelFrameSize.width),
            messageLabel.heightAnchor.constraint(equalToConstant: messageLabelFrameSize.height)
        ])
    }

    private func createOutcomeMessageBubble() -> CGPath {
        let bubblePath = UIBezierPath()
        bubblePath.move(to: CGPoint(x: bounds.maxX, y: bounds.maxY))
        bubblePath.addQuadCurve(
            to: CGPoint(
                x: bounds.maxX - Const.bubbleTailLowWidth,
                y: bounds.maxY - Const.bubbleTailLowHeight
            ),
            controlPoint: CGPoint(
                x: bounds.maxX - Const.bubbleTailLowWidth,
                y: bounds.maxY
            )
        )
        bubblePath.addQuadCurve(
            to: CGPoint(
                x: bounds.maxX - Const.two * Const.bubbleTailLowWidth,
                y: bounds.maxY
            ),
            controlPoint: CGPoint(
                x: bounds.maxX - Const.bubbleTailLowWidth,
                y: bounds.maxY
            )
        )
        bubblePath.addArc(
            withCenter: CGPoint(
                x: Const.bubbleRoundRadius,
                y: bounds.maxY - Const.bubbleRoundRadius
            ),
            radius: Const.bubbleRoundRadius,
            startAngle: Const.ninetyDegreeAngle,
            endAngle: Const.oneEightyDegreeAngle,
            clockwise: true
        )
        bubblePath.addArc(
            withCenter: CGPoint(
                x: Const.bubbleRoundRadius,
                y: Const.bubbleRoundRadius
            ),
            radius: Const.bubbleRoundRadius,
            startAngle: Const.oneEightyDegreeAngle,
            endAngle: Const.twoSeventyDegreeAngle,
            clockwise: true
        )
        bubblePath.addArc(
            withCenter: CGPoint(
                x: bounds.maxX - Const.bubbleRoundRadius - Const.bubbleTailWidth,
                y: Const.bubbleRoundRadius
            ),
            radius: Const.bubbleRoundRadius,
            startAngle: Const.twoSeventyDegreeAngle,
            endAngle: Const.zeroDegreeAngle,
            clockwise: true
        )
        bubblePath.addLine(
            to: CGPoint(
                x: bounds.maxX - Const.bubbleTailWidth,
                y: bounds.maxY - Const.bubbleTailHeight
            )
        )
        bubblePath.addQuadCurve(
            to: CGPoint(
                x: bounds.maxX,
                y: bounds.maxY
            ),
            controlPoint: CGPoint(
                x: bounds.maxX - Const.bubbleTailWidth,
                y: bounds.maxY
            )
        )
        return bubblePath.cgPath
    }

    private func createIncomeMessageBubble() -> CGPath {
        let bubblePath = UIBezierPath()
        bubblePath.move(to: CGPoint(x: bounds.minX, y: bounds.maxY))
        bubblePath.addQuadCurve(
            to: CGPoint(
                x: bounds.minX + Const.bubbleTailLowWidth,
                y: bounds.maxY - Const.bubbleTailLowHeight
            ),
            controlPoint: CGPoint(
                x: bounds.minX + Const.bubbleTailLowWidth,
                y: bounds.maxY
            )
        )
        bubblePath.addQuadCurve(
            to: CGPoint(
                x: bounds.minX + Const.two * Const.bubbleTailLowWidth,
                y: bounds.maxY
            ),
            controlPoint: CGPoint(
                x: bounds.minX + Const.bubbleTailLowWidth,
                y: bounds.maxY
            )
        )
        bubblePath.addArc(
            withCenter: CGPoint(
                x: bounds.maxX - Const.bubbleRoundRadius,
                y: bounds.maxY - Const.bubbleRoundRadius
            ),
            radius: Const.bubbleRoundRadius,
            startAngle: Const.ninetyDegreeAngle,
            endAngle: Const.zeroDegreeAngle,
            clockwise: false
        )
        bubblePath.addArc(
            withCenter: CGPoint(
                x: bounds.maxX - Const.bubbleRoundRadius,
                y: Const.bubbleRoundRadius
            ),
            radius: Const.bubbleRoundRadius,
            startAngle: Const.zeroDegreeAngle,
            endAngle: Const.twoSeventyDegreeAngle,
            clockwise: false
        )
        bubblePath.addArc(
            withCenter: CGPoint(
                x: Const.bubbleRoundRadius + Const.bubbleTailWidth,
                y: Const.bubbleRoundRadius
            ),
            radius: Const.bubbleRoundRadius,
            startAngle: Const.twoSeventyDegreeAngle,
            endAngle: Const.oneEightyDegreeAngle,
            clockwise: false
        )
        bubblePath.addLine(
            to: CGPoint(
                x: Const.bubbleTailWidth,
                y: bounds.maxY - Const.bubbleTailHeight
            )
        )
        bubblePath.addQuadCurve(
            to: CGPoint(
                x: bounds.minX,
                y: bounds.maxY
            ),
            controlPoint: CGPoint(
                x: bounds.minX + Const.bubbleTailWidth,
                y: bounds.maxY
            )
        )
        return bubblePath.cgPath
    }

    required init?(coder: NSCoder) { fatalError("init(coder:) has not been implemented") }
}
