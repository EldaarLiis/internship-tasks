//
//  MessageTray.swift
//  Lesson23
//
//  Created by Harbros 66 on 25.06.21.
//

import UIKit

private enum Const {
    static let numberOfMessages = 30
}

struct MessageTray {
    let messageArray: [MessageView]

    init() {
        messageArray = (0 ..< Const.numberOfMessages).compactMap { _ in
            MessageView()
        }
    }
}
