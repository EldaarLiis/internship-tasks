//
//  MessageModel.swift
//  Lesson23
//
//  Created by Harbros 66 on 25.06.21.
//

import UIKit

enum RandomMessages {
    static let array = [
        // long to medium long messages
        "As he waited for the shower to warm, he noticed that he could hear water change temperature.",
        "She opened up her third bottle of wine of the night.",
        "Yeah, I think it's a good environment for learning English.",
        "She insisted that cleaning out your closet was the key to good driving.",
        "Sarah ran from the serial killer holding a jug of milk.",
        "A suit of armor provides excellent sun protection on hot days.",
        "The green tea and avocado smoothie turned out exactly as would be expected.",
        "Plans for this weekend include turning wine into water.",
        "Did you go to school yesterday?",
        "The dress is too small for me.",
        "She did not cheat on the test, for it was not the right thing to do.",
        "He uses onomatopoeia as a weapon of mental destruction.",
        "Every manager should be able to recite at least ten nursery rhymes backward.",
        "The hummingbird's wings blurred while it eagerly sipped the sugar water from the feeder.",
        "Baby wipes are made of chocolate stardust.",
        "Wine goes best with beef or pork.",
        "Don't put peanut butter on the dog's nose.",
        "As the snow melted, the roads became very slippery.",
        "This cake tastes sweet.",
        "Today arrived with a crash of my car through the garage door.",
        // short messages
        "vigorous", "physical", "public", "bacon", "blade",
        "menu", "quiet", "scandal", "institution", "digress",
        "tough", "relief", "opera", "transition", "spin",
        "index", "volcano", "road", "fence", "analysis"
    ]

    static let genericMessage = "Oops. Something went wrong... I guess. But that shouldn't worry you..."
}

struct Message {
    let text: String
    let isIncoming: Bool

    init() {
        text = RandomMessages.array.randomElement() ?? RandomMessages.genericMessage
        isIncoming = .random()
    }

    init(isIncoming: Bool) {
        text = RandomMessages.array.randomElement() ?? RandomMessages.genericMessage
        self.isIncoming = isIncoming
    }

    func labelSize(font: UIFont, maxWidth: CGFloat) -> CGSize {
        let label = UILabel()
        label.font = font
        label.text = self.text
        label.frame = CGRect(origin: .zero, size: CGSize(width: maxWidth, height: .greatestFiniteMagnitude))
        label.numberOfLines = .zero
        label.lineBreakMode = .byWordWrapping
        label.sizeToFit()
        return label.frame.size
    }
}
