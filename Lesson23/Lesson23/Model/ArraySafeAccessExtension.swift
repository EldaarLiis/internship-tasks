//
//  ArraySafeAccessExtension.swift
//  Lesson23
//
//  Created by Harbros 66 on 29.06.21.
//

import Foundation

extension Array {
    func safe(at index: Int) -> Element? {
        guard self.indices.contains(index) else { return nil }
        return self[index]
    }
}
