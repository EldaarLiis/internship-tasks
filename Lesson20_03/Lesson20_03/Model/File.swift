//
//  File.swift
//  Lesson20_03
//
//  Created by Harbros 66 on 12.06.21.
//

import UIKit

enum Filetype: Int {
    case directory = 0
    case file = 1

    var color: UIColor {
        switch self {
        case .directory:
            return .blue
        case .file:
            return .green
        }
    }
}

class File {
    let type: Filetype
    let name: String
    let fulldir: String
    var fileSize: Int?

    init(type: Filetype, name: String, fulldir: String) {
        self.type = type
        self.name = name
        self.fulldir = fulldir
    }
}
