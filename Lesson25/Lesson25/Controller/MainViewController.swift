//
//  ViewController.swift
//  Lesson25
//
//  Created by Harbros 66 on 3.07.21.
//

import UIKit

private enum Const {
    static let segmentedItems = ["Web Pages", "PDF"]
    static let tableCellID = "cellID"
}

class MainViewController: UIViewController {
    private let webViewController = WebKitController()
    private let segmentedControl = UISegmentedControl(items: Const.segmentedItems)
    private let sourceTableView = UITableView()
    private let openButton = UIBarButtonItem(systemItem: .search)
    private let sourceModel = SourceModel()

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white

        navigationItem.titleView = segmentedControl
        setupSegmentedControl()

        sourceTableView.register(UITableViewCell.self, forCellReuseIdentifier: Const.tableCellID)
        view.addSubview(sourceTableView)
        sourceTableView.dataSource = self

        setupButton()
    }

    private func setupButton() {
        openButton.target = self
        openButton.action = #selector(openChosenItem)
        navigationItem.setRightBarButton(openButton, animated: false)
    }

    @objc private func openChosenItem() {
        guard
            let type = SourceType(rawValue: segmentedControl.selectedSegmentIndex),
            let chosenSourceItem = sourceTableView.indexPathForSelectedRow?.item
        else { return }
        let item: SourceItem
        switch type {
        case .webpage:
            item = sourceModel.webpageItems[chosenSourceItem]
        case .pdf:
            item = sourceModel.pdfItems[chosenSourceItem]
        }
        webViewController.load(sourceItem: item)
        navigationController?.pushViewController(webViewController, animated: true)
    }

    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        sourceTableView.frame = view.safeAreaLayoutGuide.layoutFrame
    }

    private func setupSegmentedControl() {
        segmentedControl.selectedSegmentIndex = .zero
        segmentedControl.addTarget(self, action: #selector(changeResource(_:)), for: .valueChanged)
    }

    @objc private func changeResource(_ segmentedControl: UISegmentedControl) {
        sourceTableView.reloadData()
    }
}

extension MainViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let type = SourceType(rawValue: segmentedControl.selectedSegmentIndex) else {
            return .zero
        }
        switch type {
        case .webpage:
            return sourceModel.webpageItems.count
        case .pdf:
            return sourceModel.pdfItems.count
        }
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: Const.tableCellID, for: indexPath)
        guard let type = SourceType(rawValue: segmentedControl.selectedSegmentIndex) else {
            return cell
        }
        switch type {
        case .webpage:
            guard let item = sourceModel.webpageItems.safe(at: indexPath.row) else { break }
            cell.textLabel?.text = item.title
        case .pdf:
            guard let item = sourceModel.pdfItems.safe(at: indexPath.row) else { break }
            cell.textLabel?.text = item.title
        }
        return cell
    }
}
