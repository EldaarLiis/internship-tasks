//
//  SourceModel.swift
//  Lesson25
//
//  Created by Harbros 66 on 3.07.21.
//

import Foundation

private enum Const {
    static let pdfType = ".pdf"
}

private enum SourceURLs {
    static let pdfURLs = [
        "Apollo 17 - Flight Plan": "pdf1",
        "Random Number Tables": "pdf2",
        "A Simple PDF File": "pdf3"
    ]
    static let webpageURLs = [
        "WKWebView Documentation Page": "https://developer.apple.com/documentation/webkit/wkwebview",
        "Google Doodle": "https://www.google.com/doodles",
        "Random Wiki page": "https://en.wikipedia.org/wiki/Special:Random"
    ]
}

enum SourceType: Int {
    case webpage
    case pdf
}

struct SourceModel {
    private let _pdfItems: [SourceItem?]
    var pdfItems: [SourceItem] {
        _pdfItems.compactMap { $0 }
    }
    private let _webPageItems: [SourceItem?]
    var webpageItems: [SourceItem] {
        _webPageItems.compactMap { $0 }
    }

    init() {
        _pdfItems = SourceURLs.pdfURLs.map {
            let fileURL = Bundle.main.url(forResource: $0.value, withExtension: Const.pdfType)
            return SourceItem(itemURL: fileURL, title: $0.key)
        }
        _webPageItems = SourceURLs.webpageURLs.map {
            SourceItem(itemURL: URL(string: $0.value), title: $0.key)
        }
    }
}
