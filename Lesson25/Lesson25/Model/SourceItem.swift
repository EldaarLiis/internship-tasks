//
//  SourceItem.swift
//  Lesson25
//
//  Created by Harbros 66 on 3.07.21.
//

import Foundation

struct SourceItem {
    let itemURL: URL?
    let title: String
}
