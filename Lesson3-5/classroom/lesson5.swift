//
//  lesson5.swift
//  classroom
//
//  Created by Harbros 66 on 17.05.21.
//

import Foundation

class Lesson5 {
    let arr = (1...10).map { _ in Int.random(in: 1...50) }
    func taskOne() {
        arr.forEach { print($0) }
    }
    func taskTwo() {
        let arrGreaterThan30 = arr.filter { $0 > 30 }
        print(arrGreaterThan30)
    }
    func taskThree() {
        let arrButStrings = arr.map { String($0) }
        print(arrButStrings)
    }
    func taskFour() {
        let sumOfArr = arr.reduce(0, { summ, number in summ + number })
        print(sumOfArr)
    }
    func taskFive() {
        var monthInfo = [String: (Int, Int)]()
        guard let months = DateFormatter().monthSymbols else { return }
        let daysInMonth = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]
        (0...11).forEach {
            monthInfo[months[$0]] = (daysInMonth[$0], $0 + 1)
        }
        print(monthInfo)
    }
    func taskSix() {
        var monthInfo = [String: (Int, Int)]()
        guard let months = DateFormatter().monthSymbols else { return }
        let daysInMonth = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]
        (0...11).forEach {
            monthInfo[months[$0]] = (daysInMonth[$0], $0 + 1)
        }
        monthInfo.forEach {
            print("Month \($0.key) has \($0.value.0) days and is \($0.value.1) in the year.")
        }
    }
    func taskSeven() {
        var monthInfo = [String: (Int, Int)]()
        guard let months = DateFormatter().monthSymbols else { return }
        let daysInMonth = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]
        (0...11).forEach {
            monthInfo[months[$0]] = (daysInMonth[$0], $0 + 1)
        }
        let numberOfDaysInYear = monthInfo.reduce(0, { summ, currentMonth in summ + currentMonth.value.0 })
        print(numberOfDaysInYear)
    }
}
