//
//  lesson3.swift
//  classLessons
//
//  Created by Harbros 66 on 14.05.21.
//

import Foundation

class Lesson3 {
    func taskOne() {
        let numbersOnlyString = "37525"
        let lettersAndNumbersString = "51332"
        let doubleFlat = 3
        let doublePlus = 54123565
        let optIntArray = [Int(numbersOnlyString), Int(lettersAndNumbersString), Int(doubleFlat), Int(doublePlus)]
        let intArray = createIntArrayFromOptionalIntArray(optionalIntArray: optIntArray)

        if let value = intArray.max() {
            print(value)
        }
    }
    func createIntArrayFromOptionalIntArray(optionalIntArray: [Int?]) -> [Int] {
        var intArray = [Int]()
        for optionalInt in optionalIntArray {
            if let value = optionalInt {
                intArray.append(value)
            }
        }
        return intArray
    }
    func taskTwo() {
        let john = (name: "John", age: 19, sex: "Male")
        print("\(john.name), \(john.age). \(john.sex).\n")
    }
    func taskThree() {
        let johnID = (name: "John", age: 19, sex: "Male")
        let hannahID = ("Hannah", 23, "Female")
        let timID = ("Tim", 16, "Male")
        let toidiID = ("Toidi", 15, "Other")
        let meID = ("Vitali", 20, "Helicopter")
        let personsArray: [(name: String, age: Int, sex: String)] = [johnID, hannahID, timID, toidiID, meID]
        for person in personsArray {
            outputPersonInformation(personInfo: person)
        }
    }
    func outputPersonInformation(personInfo: (name: String, age: Int, sex: String)) {
        print("\(personInfo.name), \(personInfo.age). \(personInfo.sex).\n")
    }
    func taskFour() {
        let johnID = (name: "John", age: 23, sex: "Male")
        let hannahID = ("Hannah", 23, "Female")
        whosOlder(firstPersonInfo: johnID, secondPersonInfo: hannahID)
    }
    func whosOlder(firstPersonInfo: (name: String, age: Int, sex: String),
                   secondPersonInfo: (name: String, age: Int, sex: String)) {
        if firstPersonInfo.age > secondPersonInfo.age {
            print("\(firstPersonInfo.name) is \(firstPersonInfo.age - secondPersonInfo.age)" +
                    " years older than \(secondPersonInfo.name)")
        } else if firstPersonInfo.age < secondPersonInfo.age {
            print("\(secondPersonInfo.name) is \(secondPersonInfo.age - firstPersonInfo.age)" +
                  " years older than \(firstPersonInfo.name)")
        } else {
            print("\(secondPersonInfo.name) and \(firstPersonInfo.name) are of the same age.")
        }
    }
}
