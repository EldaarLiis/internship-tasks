//
//  SingletonQueueClass.swift
//  lesson12
//
//  Created by Harbros 66 on 31.05.21.
//
typealias SQC = SingletonQueueClass

import Foundation

class SingletonQueueClass {
    static var main = SingletonQueueClass()
    let queue = DispatchQueue(label: "Singleton Queue", attributes: .concurrent)
    let group = DispatchGroup()
    private init() {}
}

class SingletonClass {
    private var studentGroup = [Student]()
    private let range = (1...Int.random(in: (1...1000)))

    func task() {
        (1...5).forEach { _ in
            studentGroup.append(Student(answer: .random(in: range), range: range))
        }

        studentGroup.forEach { student in
            SQC.main.queue.async(group: SQC.main.group) {
                student.guessTheNumber { fullname, numberOfTries in
                    print("Student \(fullname) guessed number in \(numberOfTries) times.")
                }
            }
        }

        SQC.main.group.notify(queue: .main) {
            print("Group guessed the number")
        }
    }
}
