//
//  Student.swift
//  lesson12
//
//  Created by Harbros 66 on 30.05.21.
//

import Foundation

let firstNames = ["James", "Robert", "John", "Michael", "William", "Mary", "Patricia", "Jennifer", "Linda", "Elizabeth"]
let lastNames = ["Smith", "Johnson", "Williams", "Brown", "Jones", "Miller", "Davis", "Garcia", "Wilson", "Martinez"]
// let firstNames = ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10"]

class Student {
    var answer: Int
    var range: ClosedRange<Int>

    var guessedInt: Int?
    var numberOfTries = 0
    let fullname: String

    init(answer: Int, range: ClosedRange<Int>) {
        let name = firstNames.randomElement() ?? "Ivan"
        let lastName = lastNames.randomElement() ?? "Ivanov"
        fullname = name + " " + lastName
        self.answer = answer
        self.range = range
        // print("Student \(fullname) is guessing \(self.answer).")
    }

    func guessTheNumber(completionEvent: (String, Int) -> ()) {
        sleep(1)
        repeat {
            numberOfTries += 1
            guessedInt = .random(in: range)
        } while guessedInt != answer
        // print("Student \(fullname) guessed number \(answer) in \(numberOfTries) times.")
        completionEvent(fullname, numberOfTries)
    }
}
