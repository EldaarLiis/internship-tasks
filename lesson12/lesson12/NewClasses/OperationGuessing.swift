//
//  OperationGuessing.swift
//  lesson12
//
//  Created by Harbros 66 on 2.06.21.
//
typealias OQC = OperationQueueClass

import Foundation

class OperationQueueClass {
    static let main = OperationQueueClass()
    let operationQueue = OperationQueue()
    private init() {
        operationQueue.name = "Operation Queue"
        operationQueue.maxConcurrentOperationCount = 2
    }
}

class OperationClass {
    private var studentGroup = [Student]()
    private let range = (1...Int.random(in: (1...1000)))

    func task() {
        (1...5).forEach { _ in
            studentGroup.append(Student(answer: .random(in: range), range: range))
        }

        studentGroup.forEach { student in
            OQC.main.operationQueue.addOperation {
                student.guessTheNumber { fullname, numberOfTries in
                    print("Student \(fullname) guessed number in \(numberOfTries) times.")
                }
            }
        }

        OQC.main.operationQueue.addBarrierBlock {
            print("Group guessed the number")
        }
    }
}
