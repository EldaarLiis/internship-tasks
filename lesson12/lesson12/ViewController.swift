//
//  ViewController.swift
//  lesson12
//
//  Created by Harbros 66 on 28.05.21.
//

import UIKit

class ViewController: UIViewController {
    let l12Simple = SimpleClass()
    let l12Singleton = SingletonClass()
    let l12Operation = OperationClass()

    override func viewDidLoad() {
        super.viewDidLoad()
        l12Operation.task()
        // Do any additional setup after loading the view.
    }

}
