//
//  StudentTable.swift
//  Lesson20_04
//
//  Created by Harbros 66 on 19.06.21.
//

import Foundation

private enum Const {
    static let numberOfStudents: Int = 1500 // .random(in: (5...15))
}

class StudentTable {
    private var initialGroup = [Student]()
    private var unfilteredGroup = [StudentGroup]()
    private(set) var showGroup = [StudentGroup]()
    var currentScope: ScopeButtonTypes = .date {
        didSet {
            changeScope()
        }
    }
    private var uniqueMonth = [Int]()
    private var uniqueNameStartChar = [Character]()
    private var uniqueLastNameStartChar = [Character]()

    private var searchText: String?

    init() {
        fillGroup()
        changeScope()
    }

    private func fillGroup() {
        (0 ..< Const.numberOfStudents).forEach { _ in
            initialGroup.append(Student())
        }
        initialGroup.sort {
            ($0.name, $0.lastname) < ($1.name, $1.lastname)
        }
        initialGroup.sort {
            Date.sortByMonth(lhs: $0.birthdate, rhs: $1.birthdate)
        }

        var uniqueMonthSet = Set<Int>()
        _ = initialGroup.map { $0.birthdate.getMonth }.filter { uniqueMonthSet.insert($0).inserted }
        uniqueMonth = Array(uniqueMonthSet).sorted(by: <)

        var uniqueNameStartingChars = Set<Character>()
        _ = initialGroup.compactMap { $0.name.first }.filter { uniqueNameStartingChars.insert($0).inserted }
        uniqueNameStartChar = Array(uniqueNameStartingChars).sorted(by: <)

        var uniqueLastNameStartingChars = Set<Character>()
        _ = initialGroup.compactMap { $0.lastname.first }.filter { uniqueLastNameStartingChars.insert($0).inserted }
        uniqueLastNameStartChar = Array(uniqueLastNameStartingChars).sorted(by: <)
    }

    private func changeScope() {
        unfilteredGroup = [StudentGroup]()
        switch currentScope {
        case .date:
            uniqueMonth.forEach { month in
                let group = initialGroup.filter {
                    $0.monthOfBirth == month
                }
                unfilteredGroup.append(StudentGroup(group: group))
            }
        case .name:
            uniqueNameStartChar.forEach { startingChar in
                let group = initialGroup.filter {
                    $0.name.first == startingChar
                }
                unfilteredGroup.append(StudentGroup(group: group))
            }
        case .lastname:
            uniqueLastNameStartChar.forEach { startingChar in
                let group = initialGroup.filter {
                    $0.lastname.first == startingChar
                }
                unfilteredGroup.append(StudentGroup(group: group))
            }
        }
        search(by: searchText)
    }

    var indexTitles: [String] {
        switch currentScope {
        case .date: // Date sort
            let uniqueMonthNames = uniqueMonth.map { Calendar.current.shortMonthSymbols[$0 - 1] }
            return uniqueMonthNames
        case .name: // Name sort
            return uniqueNameStartChar.map { String($0) }
        case .lastname:
            return uniqueLastNameStartChar.map { String($0) }
        }
    }

    var numberOfSections: Int {
        showGroup.count
    }

    func titleOfSection(in section: Int) -> String {
        switch currentScope {
        case .date: // Date sort
            let monthNumber = uniqueMonth.getItem(at: section)
            return Calendar.current.monthSymbols[monthNumber - 1]
        case .name: // Name sort
            return uniqueNameStartChar.getItem(at: section).description
        case .lastname:
            return uniqueLastNameStartChar.getItem(at: section).description
        }
    }

    func numberOfRows(in section: Int) -> Int {
        return showGroup[section].numberOfRows
    }

    func search(by: String?) {
        self.searchText = by
        guard let searchString = by else {
            showGroup = unfilteredGroup
            return
        }
        showGroup = [StudentGroup]()
        unfilteredGroup.forEach { studentGroup in
            let searchedGroup = studentGroup.group.filter { student in
                student.name.hasPrefix(searchString) || student.lastname.hasPrefix(searchString)
            }
            showGroup.append(StudentGroup(group: searchedGroup))
        }
    }
}
