//
//  Student.swift
//  Lesson20_01
//
//  Created by Harbros 66 on 12.06.21.
//

import UIKit

private enum Const {
    static let names = [
        "james", "robert", "john", "michael", "william", "david", "richard", "joseph", "thomas", "charles",
        "mary", "patricia", "jennifer", "linda", "elizabeth", "barbara", "susan", "jessica", "sarah", "karen"
    ]
    static let lastnames = [
        "smith", "johnson", "williams", "brown", "jones", "miller", "davis", "garcia", "rodriguez", "wilson",
        "martinez", "anderson", "taylor", "thomas", "hernandez", "moore", "martin", "jackson", "thompson", "white"
    ]

    static let oldestDate = "01.01.1980"
    static let youngestDate = "01.01.2002"
}

struct Student {
    let name: String
    let lastname: String
    var fullname: String {
        return name + " " + lastname
    }
    let birthdate: Date
    let monthOfBirth: Int

    init() {
        name = Const.names.randomElement()?.capitalized ?? "Ivan"
        lastname = Const.lastnames.randomElement()?.capitalized ?? "Ivanov"
        birthdate = .randomBetween(start: Const.oldestDate, end: Const.youngestDate)
        monthOfBirth = DateFormatter().calendar.component(.month, from: birthdate)
    }
}
