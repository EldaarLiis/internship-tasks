//
//  ViewController.swift
//  Lesson20_04
//
//  Created by Harbros 66 on 13.06.21.
//

extension Array {
    func getItem(at index: Int) -> Element {
        return self[self.index(self.startIndex, offsetBy: index)]
    }
}

import UIKit

private enum Const {
    static let cellID = "id"
    static let animationDuration: TimeInterval = 0.5
}

class ViewController: UIViewController, UITableViewDataSource, UISearchBarDelegate {
    override var prefersStatusBarHidden: Bool {
        return true
    }
    private let tableView = UITableView()
    private let searchView = UISearchBar()
    private let studentTable = StudentTable()

    override func viewDidLoad() {
        super.viewDidLoad()
        addSubviews()
        addDataSourcesAndDelegates()
    }

    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        tableView.frame = UIScreen.main.bounds
        searchView.sizeToFit()
    }

    private func addSubviews() {
        tableView.backgroundColor = .white
        view.addSubview(tableView)

        searchView.backgroundColor = .white
        searchView.showsScopeBar = true
        searchView.scopeButtonTitles = ScopeButtonTypes.Titles
        tableView.tableHeaderView = searchView
    }

    private func addDataSourcesAndDelegates() {
        tableView.dataSource = self
        searchView.delegate = self
    }

    // MARK: - Data source
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return studentTable.numberOfRows(in: section)
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: .value1, reuseIdentifier: Const.cellID)
        let student = studentTable.showGroup[indexPath.section].group[indexPath.row]

        cell.textLabel?.text = student.fullname
        cell.detailTextLabel?.text = student.birthdate.dateString()

        return cell
    }

    func numberOfSections(in tableView: UITableView) -> Int {
        return studentTable.numberOfSections
    }

    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return studentTable.titleOfSection(in: section)
    }

    func sectionIndexTitles(for tableView: UITableView) -> [String]? {
        return studentTable.indexTitles
    }

    // MARK: - SearchBar delegate
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        searchBar.setShowsCancelButton(true, animated: true)
        searchBar.becomeFirstResponder()
    }

    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        searchBar.setShowsCancelButton(false, animated: true)
        searchBar.resignFirstResponder()
    }

    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.text = .none
        studentTable.search(by: nil)
        tableView.reloadData()
        searchBar.endEditing(true)
    }

    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.endEditing(true)
    }

    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        studentTable.search(by: searchText)
        tableView.reloadData()
    }

    func searchBar(_ searchBar: UISearchBar, selectedScopeButtonIndexDidChange selectedScope: Int) {
        guard let type = ScopeButtonTypes(rawValue: searchView.selectedScopeButtonIndex) else {
            return
        }
        studentTable.currentScope = type
        tableView.reloadData()
    }
}
