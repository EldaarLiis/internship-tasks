//
//  ScopeButtonConst.swift
//  Lesson20_04
//
//  Created by Harbros 66 on 19.06.21.
//

import Foundation

enum ScopeButtonTypes: Int, CaseIterable {
    case date
    case name
    case lastname

    var description: String {
        switch self {
        case .date:
            return "Date"
        case .name:
            return "Name"
        case .lastname:
            return "Last Name"
        }
    }

    static var Titles: [String] {
        return ScopeButtonTypes.allCases.map {
            $0.description
        }
    }
}
