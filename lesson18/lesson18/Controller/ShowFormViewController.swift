//
//  ShowFormViewController.swift
//  lesson18
//
//  Created by Harbros 66 on 9.06.21.
//

import UIKit

class ShowFormViewController: UIViewController {
    override var prefersStatusBarHidden: Bool { return true }
    private let showFormView = ShowFormView()
//    private let formData: FormData

    init(_ formData: FormData) {
//        self.formData = formData
        showFormView.setupLabels(formData)
        super.init(nibName: nil, bundle: nil)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        addSubviews()
    }

    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        showFormView.frame = view.safeAreaLayoutGuide.layoutFrame
    }

    private func addSubviews() {
        showFormView.backgroundColor = .white
        view.addSubview(showFormView)
    }

    required init?(coder: NSCoder) {
//        formData = FormData()
        super.init(coder: coder)
    }
}
