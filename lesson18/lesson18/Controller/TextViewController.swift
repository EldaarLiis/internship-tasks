//
//  TextViewController.swift
//  lesson18
//
//  Created by Harbros 66 on 8.06.21.
//

import UIKit

class TextViewController: UIViewController, UISearchBarDelegate {
    override var prefersStatusBarHidden: Bool { return true }
    let textSearchView = TextSearchView()
    let textSearchModel = TextSearch()

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        addSubviews()
        textSearchView.searchBar.delegate = self
    }

    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        textSearchView.frame = view.safeAreaLayoutGuide.layoutFrame
    }

    private func addSubviews() {
        textSearchView.backgroundColor = .white
        textSearchView.textView.attributedText = textSearchModel.removeHighlight()
        view.addSubview(textSearchView)
    }

    // Search Bar delegate
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        textSearchView.textView.attributedText = textSearchModel.removeHighlight()
    }

    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        guard let textToSearch = searchBar.text else { return }
        textSearchView.textView.attributedText = textSearchModel.highlightText(textToSearch)
    }

    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.endEditing(true)
    }

    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        textSearchView.textView.attributedText = textSearchModel.removeHighlight()
    }
}
