//
//  MainMenuController.swift
//  lesson18
//
//  Created by Harbros 66 on 8.06.21.
//

import UIKit

class MainMenuController: UIViewController {
    private let buttonView = MenuButtonView()

    private let textViewController = TextViewController()
    private let formViewController = FormViewController()

    override var prefersStatusBarHidden: Bool { return true }

    override func viewDidLoad() {
        super.viewDidLoad()
        addSubviews()
        addTarget()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: animated)
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.setNavigationBarHidden(false, animated: animated)
    }

    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        buttonView.frame = UIScreen.main.bounds
    }

    private func addSubviews() {
        buttonView.backgroundColor = .white
        view.addSubview(buttonView)
    }

    private func addTarget() {
        buttonView.textViewButton.addTarget(self, action: #selector(showTextViewController), for: .touchUpInside)
        buttonView.formViewButton.addTarget(self, action: #selector(showFormViewController), for: .touchUpInside)
    }

    @objc func showTextViewController() {
        navigationController?.pushViewController(textViewController, animated: true)
    }

    @objc func showFormViewController() {
        formViewController.formView.clearForms()
        navigationController?.pushViewController(formViewController, animated: true)
    }
}
