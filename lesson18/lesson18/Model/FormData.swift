//
//  FormData.swift
//  lesson18
//
//  Created by Harbros 66 on 9.06.21.
//

import UIKit

class FormData {
    private(set) var name: String?
    private(set) var lastName: String?
    private(set) var birthDate: String?
    private(set) var email: String?
    private(set) var address: String?
    private(set) var phoneNumber: String?

    func applyData(key: String, value: String) {
        guard let keyOfValue = PossibleKeys(rawValue: key) else { return }
        switch keyOfValue {
        case .nameKey:
            name = value
        case .lastnameKey:
            lastName = value
        case .birthdateKey:
            birthDate = value
        case .emailKey:
            email = value
        case .addressKey:
            address = value
        case .phoneKey:
            phoneNumber = value
        }
    }
}
