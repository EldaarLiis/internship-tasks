//
//  ShowFormView.swift
//  lesson18
//
//  Created by Harbros 66 on 9.06.21.
//

import UIKit

private enum StyleConstants {
    static let textAlignment: NSTextAlignment = .center
    static let labelColor: UIColor = .black
    static let labelBorderWidth: CGFloat = 1
    static let labelBorderColor: CGColor = UIColor.gray.cgColor
    static let labelBorderCornerRadius: CGFloat = 5
}

class ShowFormView: UIView {
    private typealias Style = StyleConstants

    private let nameLabel = UILabel()
    private let lastnameLabel = UILabel()
    private let birthdateLabel = UILabel()
    private let emailLabel = UILabel()
    private let addressLabel = UILabel()
    private let phoneNumberLabel = UILabel()

    init() {
        super.init(frame: .zero)
        addSubviews()
    }

    override func layoutSubviews() {
        for view in subviews as [UIView] {
            if let label = view as? UILabel {
                label.frame = ViewSize.fieldRect(frame)
            }
        }

        nameLabel.center = ViewSize.getRectCenter(on: frame, by: ViewPosition.name.info)
        lastnameLabel.center = ViewSize.getRectCenter(on: frame, by: ViewPosition.lastname.info)
        birthdateLabel.center = ViewSize.getRectCenter(on: frame, by: ViewPosition.birthdate.info)
        emailLabel.center = ViewSize.getRectCenter(on: frame, by: ViewPosition.email.info)
        addressLabel.center = ViewSize.getRectCenter(on: frame, by: ViewPosition.address.info)
        phoneNumberLabel.center = ViewSize.getRectCenter(on: frame, by: ViewPosition.number.info)
    }

    private func addSubviews() {
        addSubview(nameLabel)
        addSubview(lastnameLabel)
        addSubview(birthdateLabel)
        addSubview(emailLabel)
        addSubview(addressLabel)
        addSubview(phoneNumberLabel)

        for view in subviews as [UIView] {
            if let label = view as? UILabel {
                label.textColor = Style.labelColor
                label.textAlignment = Style.textAlignment
                label.layer.cornerRadius = Style.labelBorderCornerRadius
                label.layer.borderWidth = Style.labelBorderWidth
                label.layer.borderColor = Style.labelBorderColor
            }
        }
    }

    func setupLabels(_ formData: FormData) {
        nameLabel.text = formData.name
        lastnameLabel.text = formData.lastName
        birthdateLabel.text = formData.birthDate
        emailLabel.text = formData.email
        addressLabel.text = formData.address
        phoneNumberLabel.text = formData.phoneNumber
    }

    required init?(coder: NSCoder) { super.init(coder: coder) }
}
