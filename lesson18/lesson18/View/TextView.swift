//
//  TextView.swift
//  lesson18
//
//  Created by Harbros 66 on 8.06.21.
//

import UIKit
private enum Const {
    static let searchBarRectHeight: CGFloat = 40
}

private enum StyleConstants {
    static let searchBarPlaceholder = "What to search"
    static let isTextViewEditable = false
}

class TextSearchView: UIView {
    private typealias Style = StyleConstants
    private(set) var textView = UITextView()
    private(set) var searchBar = UISearchBar()

    init() {
        super.init(frame: .zero)
        addSubviews()
    }

    override func layoutSubviews() {
        searchBar.frame = CGRect(
            origin: .zero,
            size: CGSize(
                width: frame.width,
                height: Const.searchBarRectHeight
            )
        )
        textView.frame = CGRect(
            x: .zero,
            y: Const.searchBarRectHeight,
            width: frame.width,
            height: frame.height - Const.searchBarRectHeight
        )
    }

    private func addSubviews() {
        searchBar.placeholder = Style.searchBarPlaceholder
        addSubview(searchBar)

        textView.isEditable = Style.isTextViewEditable
        addSubview(textView)
    }

    required init?(coder: NSCoder) { super.init(coder: coder) }
}
