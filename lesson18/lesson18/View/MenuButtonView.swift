//
//  MainMenuView.swift
//  lesson18
//
//  Created by Harbros 66 on 8.06.21.
//

import UIKit

private enum ButtonConstants {
    static let buttonSize = CGSize(
        width: 150,
        height: 30
    )
    static let firstButtonOffset: CGPoint = .zero
    static let secondButtonOffset = CGPoint(x: .zero, y: buttonSize.height * 2)

    static let buttonViewOffsetFromCenter = CGPoint(
        x: buttonSize.width / 2,
        y: buttonSize.height * 1.5
    )
}

private enum StyleConstants {
    static let textViewButtonLabelText = "Show text view."
    static let textViewButtonLabelColor: UIColor = .black

    static let formViewButtonLabelText = "Show form view."
    static let formViewButtonLabelColor: UIColor = .black
}

class MenuButtonView: UIView {
    private typealias Const = ButtonConstants
    private typealias Style = StyleConstants
    private(set) var textViewButton = UIButton()
    private(set) var formViewButton = UIButton()

    init() {
        super.init(frame: .zero)
        styleButtons()
        addSubviews()
    }

    override func layoutSubviews() {
        textViewButton.frame = CGRect(
            origin: getOffset(frame, offset: Const.firstButtonOffset),
            size: Const.buttonSize
        )
        formViewButton.frame = CGRect(
            origin: getOffset(frame, offset: Const.secondButtonOffset),
            size: Const.buttonSize
        )
    }

    private func addSubviews() {
        addSubview(textViewButton)
        addSubview(formViewButton)
    }

    private func styleButtons() {
        textViewButton.setTitle(Style.textViewButtonLabelText, for: .normal)
        textViewButton.setTitleColor(Style.textViewButtonLabelColor, for: .normal)

        formViewButton.setTitle(Style.formViewButtonLabelText, for: .normal)
        formViewButton.setTitleColor(Style.formViewButtonLabelColor, for: .normal)
    }

    private func getOffset(_ frame: CGRect, offset: CGPoint = .zero) -> CGPoint {
        return CGPoint(
            x: frame.midX - Const.buttonViewOffsetFromCenter.x + offset.x,
            y: frame.midY - Const.buttonViewOffsetFromCenter.y + offset.y
        )
    }

    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
}
