//
//  PopoverTableViewController.swift
//  Lesson24
//
//  Created by Harbros 66 on 30.06.21.
//

import UIKit

private enum Const {
    static let tableViewWidth: CGFloat = 200
    static let tableViewCellID = "PopoverTableViewCellID"
    static let fontSize: CGFloat = 14
    static let rowHeightConstantToFontSize: CGFloat = 4
}

class PopoverTableViewController: UITableViewController {
    let studentModel: [String]
    init(student: StudentWithAddress) {
        studentModel = student.translateToArray()
        super.init(nibName: nil, bundle: nil)
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: Const.tableViewCellID)
        view.backgroundColor = .white
        tableView.allowsSelection = false
    }

    override func viewWillLayoutSubviews() {
        preferredContentSize = CGSize(width: Const.tableViewWidth, height: tableView.contentSize.height)
    }

    override func numberOfSections(in tableView: UITableView) -> Int { 1 }
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        studentModel.count
    }
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: Const.tableViewCellID, for: indexPath)
        cell.textLabel?.text = studentModel[indexPath.item]
        cell.textLabel?.font = cell.textLabel?.font.withSize(Const.fontSize)
        return cell
    }
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return Const.fontSize + Const.rowHeightConstantToFontSize
    }

    required init?(coder: NSCoder) { fatalError("init(coder:) has not been implemented") }
}
