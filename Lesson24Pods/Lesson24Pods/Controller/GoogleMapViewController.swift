//
//  GoogleMapViewController.swift
//  Lesson24
//
//  Created by Harbros 66 on 1.07.21.
//

import GoogleMaps
import UIKit

private enum Const {
    static let googleCameraZoom: Float = 15

    static let iconSideMultiplier: CGFloat = 0.1

    static let attendanceTableCellID = "attendanceID"
    static let addressGetError = "No address found"

    static let firstCircleRadius: CLLocationDistance = 1000
    static let secondCircleRadius: CLLocationDistance = 1500
    static let thirdCircleRadius: CLLocationDistance = 3000

    static let numberOfCircles = 3
    static let circleFillColor: UIColor = .systemBlue
    static let firstCircleFillAlpha: CGFloat = 0.2
    static let secondCircleFillAlpha: CGFloat = 0.10
    static let thirdCircleFillAlpha: CGFloat = 0.05

    static let tableViewOpacity: CGFloat = 0.4
    static let tableViewOffsetFromBorders: CGFloat = 15
    static let tableViewWidthOfView: CGFloat = 0.5

    static let numberOfRowsInAttendanceTable = 4

    static let tableFontSize: CGFloat = 14
    static let tableRowAddToFont: CGFloat = 4
    static var tableRowHeight: CGFloat {
        tableFontSize + tableRowAddToFont
    }
}

class GoogleMapViewController: UIViewController {
    override var prefersStatusBarHidden: Bool { true }
    private let studentModel: StudentGroup
    private let meetupPlace = MeetupPlace()
    private let meetupInfo = MeetupInfo()

    private let mapView = GMSMapView()
    private let geocoder = GMSGeocoder()
    private let zoomButton = UIBarButtonItem(systemItem: .search)

    private let attendanceTableView = UITableView()

    private var iconSide: CGFloat {
        min(view.frame.width, view.frame.height) * Const.iconSideMultiplier
    }
    private var circles: [GMSCircle]

    init(studentModel: StudentGroup) {
        self.studentModel = studentModel
        circles = (0 ..< Const.numberOfCircles).map { _ in
            GMSCircle()
        }
        super.init(nibName: nil, bundle: nil)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white

        setupButton()

        mapView.delegate = self
        addStudentMarkers()
        addMeetupMarker()
        initCircles(centerOfAnnotation: meetupPlace.coordinate)
        view.addSubview(mapView)

        meetupInfo.fill(meetup: meetupPlace, studentGroup: studentModel)
        setupTable()
        view.addSubview(attendanceTableView)
        addAttendanceTableViewConstraints()
    }

    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        mapView.frame = view.safeAreaLayoutGuide.layoutFrame
    }

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        actionZoomToFit()
    }

    private func setupButton() {
        zoomButton.target = self
        zoomButton.action = #selector(actionZoomToFit)

        navigationItem.setRightBarButton(zoomButton, animated: false)
    }

    @objc private func actionZoomToFit() {
        var bounds = GMSCoordinateBounds(
            coordinate: studentModel.centerCoordinate,
            coordinate: studentModel.centerCoordinate
        )
        studentModel.studentGroup.forEach { student in
            bounds = bounds.includingCoordinate(student.coordinate)
        }

        mapView.animate(with: GMSCameraUpdate.fit(bounds))
    }

    private func addStudentMarkers() {
        studentModel.studentGroup.forEach { student in
            let studentMarker = GMSMarker()
            studentMarker.position = student.coordinate
            studentMarker.title = student.title
            studentMarker.snippet = student.subtitle
            studentMarker.map = mapView
            studentMarker.userData = student

            let studentImageView = UIImageView(image: student.markerImage)
            studentMarker.iconView = studentImageView
            studentImageView.translatesAutoresizingMaskIntoConstraints = false
            NSLayoutConstraint.activate([
                studentImageView.widthAnchor.constraint(equalToConstant: iconSide),
                studentImageView.heightAnchor.constraint(equalToConstant: iconSide)
            ])
        }
    }

    private func addMeetupMarker() {
        let meetupMarker = GMSMarker()
        meetupMarker.position = meetupPlace.coordinate
        meetupMarker.title = meetupPlace.title
        meetupMarker.map = mapView
        meetupMarker.userData = meetupPlace
        meetupMarker.isDraggable = true

        let meetupImageView = UIImageView(image: meetupPlace.markerImage)
        meetupMarker.iconView = meetupImageView
        meetupImageView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            meetupImageView.widthAnchor.constraint(equalToConstant: iconSide),
            meetupImageView.heightAnchor.constraint(equalToConstant: iconSide)
        ])
    }

    private func initCircles(centerOfAnnotation: CLLocationCoordinate2D) {
        circles[0] = GMSCircle(position: centerOfAnnotation, radius: Const.firstCircleRadius)
        circles[1] = GMSCircle(position: centerOfAnnotation, radius: Const.secondCircleRadius)
        circles[2] = GMSCircle(position: centerOfAnnotation, radius: Const.thirdCircleRadius)

        circles[0].fillColor = Const.circleFillColor.withAlphaComponent(Const.firstCircleFillAlpha)
        circles[1].fillColor = Const.circleFillColor.withAlphaComponent(Const.secondCircleFillAlpha)
        circles[2].fillColor = Const.circleFillColor.withAlphaComponent(Const.thirdCircleFillAlpha)

        circles.forEach {
            $0.map = mapView
            $0.strokeWidth = .zero
        }
    }

    private func drawCircles(centerOfAnnotation: CLLocationCoordinate2D) {
        circles.forEach {
            $0.position = centerOfAnnotation
            $0.map = mapView
        }
    }

    private func removeCircles() {
        circles.forEach { $0.map = nil }
    }

    private func addAttendanceTableViewConstraints() {
        attendanceTableView.translatesAutoresizingMaskIntoConstraints = false
        let tableViewHeight = Const.tableRowHeight * CGFloat(Const.numberOfRowsInAttendanceTable)
        NSLayoutConstraint.activate([
            attendanceTableView.trailingAnchor.constraint(
                equalTo: view.trailingAnchor,
                constant: -Const.tableViewOffsetFromBorders
            ),
            attendanceTableView.bottomAnchor.constraint(
                equalTo: view.bottomAnchor,
                constant: -Const.tableViewOffsetFromBorders
            ),
            attendanceTableView.widthAnchor.constraint(
                equalTo: view.widthAnchor,
                multiplier: Const.tableViewWidthOfView
            ),
            attendanceTableView.heightAnchor.constraint(
                equalToConstant: tableViewHeight
            )
        ])
    }

    private func setupTable() {
        attendanceTableView.layoutMargins = .zero
        attendanceTableView.backgroundColor = .clear
        attendanceTableView.register(AttendanceTableCell.self, forCellReuseIdentifier: Const.attendanceTableCellID)
        attendanceTableView.isScrollEnabled = false
        attendanceTableView.allowsSelection = false
        attendanceTableView.delegate = self
        attendanceTableView.dataSource = self
    }

    required init?(coder: NSCoder) { fatalError("init(coder:) has not been implemented") }
}

extension GoogleMapViewController: GMSMapViewDelegate {
    func mapView(_ mapView: GMSMapView, didTapInfoWindowOf marker: GMSMarker) {
        guard let student = marker.userData as? Student else { return }
        geocoder.reverseGeocodeCoordinate(student.coordinate) { placemarks, error in
            guard
                error == nil,
                let closestPlacemark = placemarks?.firstResult()
            else {
                print(Const.addressGetError)
                return
            }
            let pinStudent = StudentWithAddress(placemark: closestPlacemark, student: student)
            let popVC = PopoverTableViewController(student: pinStudent)
            popVC.modalPresentationStyle = .popover
            let popoverVC = popVC.popoverPresentationController
            popoverVC?.delegate = self
            popoverVC?.sourceView = self.view
            popoverVC?.sourceRect = CGRect(
                x: self.view.bounds.midX,
                y: self.view.bounds.maxY,
                width: .zero,
                height: .zero
            )
            self.present(popVC, animated: true, completion: nil)
        }
    }

    func mapView(_ mapView: GMSMapView, didBeginDragging marker: GMSMarker) {
        guard marker.userData is MeetupPlace else { return }
        removeCircles()
    }

    func mapView(_ mapView: GMSMapView, didEndDragging marker: GMSMarker) {
        guard marker.userData is MeetupPlace else { return }
        drawCircles(centerOfAnnotation: marker.position)
        meetupPlace.coordinate = marker.position
        meetupInfo.fill(meetup: meetupPlace, studentGroup: studentModel)
        attendanceTableView.reloadData()
    }
}

extension GoogleMapViewController: UIPopoverPresentationControllerDelegate {
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        return .none
    }
}

extension GoogleMapViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return Const.tableRowHeight
    }
}

extension GoogleMapViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return Const.numberOfRowsInAttendanceTable
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: Const.attendanceTableCellID, for: indexPath)
        guard let attendanceCell = cell as? AttendanceTableCell else { return cell }
        switch indexPath.item {
        case 0:
            attendanceCell.setupCell(
                lower: .zero,
                upper: Const.firstCircleRadius,
                studentCounter: meetupInfo.checkNumberInRadius(lowerBoundRadius:upperBoundRadius:)
            )
        case 1:
            attendanceCell.setupCell(
                lower: Const.firstCircleRadius,
                upper: Const.secondCircleRadius,
                studentCounter: meetupInfo.checkNumberInRadius(lowerBoundRadius:upperBoundRadius:)
            )
        case 2:
            attendanceCell.setupCell(
                lower: Const.secondCircleRadius,
                upper: Const.thirdCircleRadius,
                studentCounter: meetupInfo.checkNumberInRadius(lowerBoundRadius:upperBoundRadius:)
            )
        case 3:
            attendanceCell.setupCell(
                lower: Const.thirdCircleRadius,
                upper: .infinity,
                studentCounter: meetupInfo.checkNumberInRadius(lowerBoundRadius:upperBoundRadius:)
            )
        default:
            break
        }

        cell.layoutMargins = .zero
        cell.textLabel?.font = cell.textLabel?.font.withSize(Const.tableFontSize)
        cell.backgroundColor = .white.withAlphaComponent(Const.tableViewOpacity)
        return cell
    }
}
