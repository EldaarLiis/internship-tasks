//
//  MeetupInfo.swift
//  Lesson24
//
//  Created by Harbros 66 on 1.07.21.
//

import MapKit

private enum Const {
    static let chanceFalldownOnDistance: Double = 1.1

    // exponent function consts
    static let metersInKM: Double = 1000
    static let bConst: Double = 0.5
    static let bCoeff: Double = 0.25
    static let xOffset: Double = 2
}

class MeetupInfo {
    private var meetupCoordinate: CLLocation?
    private var studentsCoordinates: [CLLocation]?
    private var distanceTable: [CLLocationDistance]?

    func fill(meetup: MeetupPlace, studentGroup: StudentGroup) {
        meetupCoordinate = CLLocation(coordinate: meetup.coordinate)
        studentsCoordinates = studentGroup.studentGroup.compactMap { student in
            CLLocation(coordinate: student.coordinate)
        }
        distanceTable = studentsCoordinates?.compactMap { studentCoord in
            studentCoord.distance(fromOptional: meetupCoordinate)
        }
    }

    func checkNumberInRadius(lowerBoundRadius: CLLocationDistance, upperBoundRadius: CLLocationDistance) -> Int {
        guard let distanceTable = distanceTable else { return .zero }
        return distanceTable.filter {
            guard
                $0 >= lowerBoundRadius,
                $0 < upperBoundRadius
            else {
                return false
            }
            return true
        }.count
    }

    // random exponential generator with cute function :)
    func checkAvailability(student: Student) -> Bool {
        let optDistance = CLLocation(coordinate: student.coordinate)?.distance(fromOptional: meetupCoordinate)
        guard let distance = optDistance else {
            return false
        }
        let chance = Const.bCoeff
            * pow(Const.bConst, Const.chanceFalldownOnDistance * distance / Const.metersInKM - Const.xOffset)
        return Double.random(in: (0...1)).isLess(than: chance)
    }
}
