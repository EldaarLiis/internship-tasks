//
//  StudentGroup.swift
//  Lesson24
//
//  Created by Harbros 66 on 30.06.21.
//

import MapKit

private enum Const {
    static let numberOfStudents = 10

    static let generationRadiusInAngle: CLLocationDegrees = 0.02 // 0.01 ~ 1km
    static var generationOffsetRange: ClosedRange<CLLocationDegrees> {
        (-generationRadiusInAngle ... generationRadiusInAngle)
    }
    // 52.09697023288286, 23.742266242229157 - Domus Coordinate (by Google) :)
    static let domusCoordinate = CLLocationCoordinate2D(latitude: 52.09697023288286, longitude: 23.742266242229157)
}

class StudentGroup {
    let studentGroup: [Student]
    let centerCoordinate: CLLocationCoordinate2D

    init(centerCoordinate: CLLocationCoordinate2D = Const.domusCoordinate) {
        self.centerCoordinate = centerCoordinate

        studentGroup = (0 ..< Const.numberOfStudents).map { _ in
            let studentLatitude = centerCoordinate.latitude + .random(in: Const.generationOffsetRange)
            let studentLongitude = centerCoordinate.longitude + .random(in: Const.generationOffsetRange)
            let studentCoordinate = CLLocationCoordinate2D(latitude: studentLatitude, longitude: studentLongitude)
            return Student(coordinate: studentCoordinate)
        }
    }
}
