//
//  AttendanceTableCell.swift
//  Lesson24
//
//  Created by Harbros 66 on 1.07.21.
//

import MapKit

class AttendanceTableCell: UITableViewCell {
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: .value1, reuseIdentifier: reuseIdentifier)
    }

    func setupCell(
        lower: CLLocationDistance,
        upper: CLLocationDistance,
        studentCounter: (CLLocationDistance, CLLocationDistance) -> Int
    ) {
        let numberOfStudentsInRange = studentCounter(lower, upper).description
        let lowerBound = "\((lower / 1000).description)km"
        let upperBound = upper != .infinity
            ? "\((upper / 1000).description)km"
            : "inf"
        textLabel?.text = "\(numberOfStudentsInRange) \tin \(lowerBound)-\(upperBound)"
    }

    required init?(coder: NSCoder) { fatalError("init(coder:) has not been implemented") }
}
