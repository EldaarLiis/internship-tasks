//
//  MeetupPlace.swift
//  Lesson24
//
//  Created by Harbros 66 on 30.06.21.
//

import MapKit

private enum Const {
    static let meetupImageName = "meetup"
    static let meetupGoogleImageName = "meetupMarker"
    static let meetupTitle = "Really neccessary stuff"
    static let domusCoordinate = CLLocationCoordinate2D(latitude: 52.09697023288286, longitude: 23.742266242229157)
}

class MeetupPlace: NSObject, MKAnnotation {
    var coordinate: CLLocationCoordinate2D
    var image = UIImage(named: Const.meetupImageName)
    var markerImage = UIImage(named: Const.meetupGoogleImageName)
    var title: String? {
        Const.meetupTitle
    }

    init(coordinate: CLLocationCoordinate2D = Const.domusCoordinate) {
        self.coordinate = coordinate
    }
}
