//
//  DateExtension.swift
//  Lesson20_04
//
//  Created by Harbros 66 on 13.06.21.
//

import Foundation

extension Date {
    static func randomBetween(start: String, end: String, format: String = "dd.MM.yyyy") -> Date {
        let date1 = Date.parse(start, format: format)
        let date2 = Date.parse(end, format: format)
        return Date.randomBetween(start: date1, end: date2)
    }

    static func randomBetween(start: Date, end: Date) -> Date {
        let date1 = min(start, end)
        let date2 = max(start, end)
        let span = TimeInterval.random(in: date1.timeIntervalSinceNow...date2.timeIntervalSinceNow)
        return Date(timeIntervalSinceNow: span)
    }

    static func parse(_ string: String, format: String = "dd.MM.yyyy") -> Date {
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = NSTimeZone.default
        dateFormatter.dateFormat = format

        guard let date = dateFormatter.date(from: string) else { return .init() }
        return date
    }

    func dateString(_ format: String = "dd.MM.yyyy") -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        return dateFormatter.string(from: self)
    }
}
