//
//  FullAddress.swift
//  Lesson24
//
//  Created by Harbros 66 on 30.06.21.
//

import GoogleMaps
import MapKit

class StudentWithAddress {
    let student: Student
    let address1: String?
    let city: String?
    let country: String?

    init(placemark: CLPlacemark, student: Student) {
        self.student = student
        address1 = placemark.thoroughfare
        city = placemark.locality
        country = placemark.country
    }

    init(placemark: GMSAddress, student: Student) {
        self.student = student
        address1 = placemark.thoroughfare
        city = placemark.locality
        country = placemark.country
    }

    func translateToArray() -> [String] {
        let studentInfoArray = [
            "Full Name: \(student.name) \(student.lastname)",
            "Birthday: \(student.birthday.dateString())",
            "Sex: \(student.sex.rawValue)",
            "Country: \(country ?? "")",
            "City: \(city ?? "")",
            "Street: \(address1 ?? "")"
        ]
        return studentInfoArray
    }

    func printSelf() {
        print("""
        address1:   \(address1 ?? "")
        city:       \(city ?? "")
        country:    \(country ?? "")
        """)
    }
}
