//
//  ViewController.swift
//  lesson6
//
//  Created by Harbros 66 on 18.05.21.
//

import UIKit
class ViewController: UIViewController {
    let l6ClassesObject = Lesson6Classes()
    let l6StructsObject = Lesson6Structs()
    override func viewDidLoad() {
        super.viewDidLoad()
        // l6ClassesObject.taskThree()
        l6StructsObject.taskFour()
        // Do any additional setup after loading the view.
    }
}
