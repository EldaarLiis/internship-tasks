//
//  workers.swift
//  lesson6
//
//  Created by Harbros 66 on 18.05.21.
//

import Foundation

class WorkerClass {
    var workerName: String
    let workerSurname: String
    var workerIsWorking = Bool.random()
    init(name: String, surname: String) {
        workerName = name
        workerSurname = surname
    }
}
class Lesson6Classes {
    func createOfficeClass() -> [WorkerClass] {
        let johnWorker = WorkerClass(name: "John", surname: "Bolt")
        let hannahWorker = WorkerClass(name: "Hannah", surname: "Spring")
        let koreyWorker = WorkerClass(name: "Korey", surname: "Train")
        let kiteWorker = WorkerClass(name: "Kite", surname: "Bowl")
        let jimWorker = WorkerClass(name: "Jim", surname: "Carrey")
        return [johnWorker, hannahWorker, koreyWorker, kiteWorker, jimWorker]
    }
    // prints workers info
    func printOffice(office: [WorkerClass]) {
        office.forEach {
            print("\($0.workerName) \($0.workerSurname) \($0.workerIsWorking ? "is" : "isn't") working" )
        }
    }
    func taskOne() {
        let office = createOfficeClass()
        printOffice(office: office)
    }
    func taskTwo() {
        let office = createOfficeClass()
        office.filter { !$0.workerIsWorking }
            .forEach {
                $0.workerIsWorking.toggle()
                print("\($0.workerName) is now working")
            }
    }
    func taskThree() {
        let office = createOfficeClass()
        let sortedOffice = office.filter { $0.workerIsWorking }.sorted { $0.workerSurname < $1.workerSurname }
            + office.filter { !$0.workerIsWorking } .sorted { $0.workerName < $1.workerName }
        printOffice(office: sortedOffice)
    }
    func taskFour() {
        let office = createOfficeClass()
        print("Before Changes:")
        printOffice(office: office)
        let newOffice = office
        newOffice[0].workerName = "XXX"
        newOffice[1].workerName = "YYY"
        print("\nAfter Changes\nOld Office:")
        printOffice(office: office)
        print("\nNew Office:")
        printOffice(office: newOffice)
    }
}
