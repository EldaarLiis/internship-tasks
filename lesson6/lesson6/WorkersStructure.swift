//
//  WorkersStructure.swift
//  lesson6
//
//  Created by Harbros 66 on 18.05.21.
//

import Foundation

struct WorkerStruct {
    var workerName: String
    var workerSurname: String
    var workerIsWorking = Bool.random()
    init(name: String, surname: String) {
        workerName = name
        workerSurname = surname
    }
}
class Lesson6Structs {
    func createOfficeStruct() -> [WorkerStruct] {
        let johnWorker = WorkerStruct(name: "John", surname: "Bolt")
        let hannahWorker = WorkerStruct(name: "Hannah", surname: "Spring")
        let koreyWorker = WorkerStruct(name: "Korey", surname: "Train")
        let kiteWorker = WorkerStruct(name: "Kite", surname: "Bowl")
        let jimWorker = WorkerStruct(name: "Jim", surname: "Carrey")
        return [johnWorker, hannahWorker, koreyWorker, kiteWorker, jimWorker]
    }
    func printOffice(office: [WorkerStruct]) {
        office.forEach {
            print("\($0.workerName) \($0.workerSurname) \($0.workerIsWorking ? "is" : "isn't") working" )
        }
    }
    func taskOne() {
        let office = createOfficeStruct()
        printOffice(office: office)
    }
    func taskTwo() {
        var office = createOfficeStruct()
        for index in office.indices where !office[index].workerIsWorking {
                office[index].workerIsWorking.toggle()
                print("\(office[index].workerName) is now working")
            }
    }
    func taskThree() {
        let office = createOfficeStruct()
        let sortedOffice = office.filter { $0.workerIsWorking } .sorted { $0.workerSurname < $1.workerSurname }
            + office.filter { !$0.workerIsWorking } .sorted { $0.workerName < $1.workerName }
        printOffice(office: sortedOffice)
    }
    func taskFour() {
        let office = createOfficeStruct()
        print("Before Changes:")
        printOffice(office: office)
        var newOffice = office
        newOffice[0].workerName = "XXX"
        newOffice[1].workerName = "YYY"
        print("\nAfter Changes\nOld Office:")
        printOffice(office: office)
        print("\nNew Office:")
        printOffice(office: newOffice)
    }
}
