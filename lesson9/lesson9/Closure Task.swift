//
//  Closure Task.swift
//  lesson9
//
//  Created by Harbros 66 on 23.05.21.
//

import Foundation

class Lesson9ClosureTask {
    func closureTask() {
        /*emptyClosurecounting {
            print("Call in outer closure.")
        }

        intArraySorting()*/

        let intArray = (1...10).map { _ in Int.random(in: 1...100) }
        // to be sure - i wrote functions that do what they should be doing
        let max = minMaxArray(intArray: intArray, closure: bigger)
        let min = minMaxArray(intArray: intArray, closure: smaller)
        // and here is implementation with closure
        let minClosure = minMaxArray(intArray: intArray) {
            guard let currMin = $1 else {
                return true
            }

            return $0 < currMin
        }
        let maxClosure = minMaxArray(intArray: intArray) {
            guard let currMax = $1 else {
                return true
            }

            return $0 > currMax
        }
        // results should be the same
        print(max ?? "No max", maxClosure ?? "No max")
        print(min ?? "No max", minClosure ?? "No max")

        randomStringSorting()
    }

    func emptyClosurecounting(emptyclosure: () -> ()) {
        (1...10).forEach {
            print("Counting \($0) in inner closure.")
        }
        emptyclosure()
    }

    func intArraySorting() {
        let intArray = (1...10).map { _ in Int.random(in: 1...100) }
        print(
            "Increasing order: ",
            intArray.sorted(by: <),
            "\nDecreasing order: ",
            intArray.sorted(by: >)
        )
    }

    func minMaxArray(intArray: [Int], closure: (_ int: Int, _ optionalInt: Int?) -> Bool) -> Int? {
        var innerOptionalInt: Int?
        intArray.forEach {
            let result = closure($0, innerOptionalInt)
            if result {
                innerOptionalInt = $0
            }
        }
        return innerOptionalInt
    }

    func bigger(nextItem: Int, max: Int?) -> Bool {
        // if we dont have current max -> we mark passed item as max via returning true
        guard let currMax = max else {
            return true
        }

        // if we have current max -> we should compare it to passed item
        // if passed item is bigger -> we mark it as max via returning true
        // else -> we return false
        return nextItem > currMax
    }

    func smaller(nextItem: Int, min: Int?) -> Bool {
        // if we dont have current min -> we mark passed item as min via returning true
        guard let currMin = min else {
            return true
        }

        // if we have current min -> we should compare it to passed item
        // if passed item is smaller -> we mark it as min via returning true
        // else -> we return false
        return nextItem < currMin
    }

    func randomStringSorting() {
        let randomString = "aeioubcdfghjklmnpqrstvwxyz123456789.;'[]_+!@#$%".shuffled()
        print(String(randomString))
        // aeioubcdfghjklmnpqrstvwxyz123456789.;'[]_+!@#$%
        let vowels = "aeiou"
        let sortedString = randomString.sorted { currChar, nextChar in
            if currChar.isLetter {
                if nextChar.isLetter {
                    if vowels.contains(currChar) && vowels.contains(nextChar) {
                        return currChar < nextChar
                    } else if vowels.contains(currChar) || vowels.contains(nextChar) {
                        return vowels.contains(currChar)
                    } else {
                        return currChar < nextChar
                    }
                }
            } else if currChar.isNumber {
                if nextChar.isLetter {
                    return false
                } else if nextChar.isNumber {
                    return currChar < nextChar
                }
            } else {
                if nextChar.isNumber || nextChar.isLetter {
                    return false
                } else {
                    return currChar < nextChar
                }
            }
            return true
        }

        print(String(sortedString))
    }
}
