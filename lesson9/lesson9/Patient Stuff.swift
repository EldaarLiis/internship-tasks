//
//  Delegates.swift
//  lesson9
//
//  Created by Harbros 66 on 21.05.21.
//

import Foundation

enum BodyPart: Int {
    case head = 1
    case breast = 2
    case abdomen = 3
    case limb = 4
}

class Patient {
    let name: String
    var temperature: Double
    weak var doctorDelegate: PatientDelegate?
    var bodypartInPain: [BodyPart]
    var grade: Int?

    init(patientName: String, patientTemperature: Double, bodypartHurts: [BodyPart]) {
        name = patientName
        temperature = patientTemperature
        bodypartInPain = bodypartHurts
    }

    func patentStateWorsened() {
        print("-------------")
        self.doctorDelegate?.checkPatient(patientToCheck: self)
        self.doctorDelegate?.treatPatient(patientToTreat: self)
        grade = .random(in: 0...10)
    }
}
