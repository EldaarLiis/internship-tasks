//
//  ViewController.swift
//  lesson9
//
//  Created by Harbros 66 on 21.05.21.
//

import UIKit

class ViewController: UIViewController {

    let l9DT = Lesson9DelegateTask()
    let l9CT = Lesson9ClosureTask()
    override func viewDidLoad() {
        super.viewDidLoad()
        // l9DT.delegateTask()
        l9CT.closureTask()
        // Do any additional setup after loading the view.
    }

}
