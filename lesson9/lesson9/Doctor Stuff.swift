//
//  Doctor Stuff.swift
//  lesson9
//
//  Created by Harbros 66 on 23.05.21.
//

import Foundation

protocol PatientDelegate: AnyObject {
    func checkPatient(patientToCheck: Patient)
    func treatPatient(patientToTreat: Patient)
    var report: [(name: String, bodypartsHurt: BodyPart)] { get }
    func dayendReport()
}

class GoodDoctor: PatientDelegate {
    var report: [(name: String, bodypartsHurt: BodyPart)] = []

    func checkPatient(patientToCheck: Patient) {
        if patientToCheck.temperature > 40 {
            print("\(patientToCheck.name) has flu, should be treated with an injection.")
        } else if patientToCheck.temperature > 37 {
            print("\(patientToCheck.name) has cold, should be treated with hot tea and some pills.")
        } else {
            print("\(patientToCheck.name)'s troubles have nothing to do with temperature.")
        }

        patientToCheck.bodypartInPain.forEach {
            report.append((name: patientToCheck.name, bodypartsHurt: $0))
            switch $0 {
            case .abdomen:
                print("\(patientToCheck.name) has abdomen pains, should take some special medicine.")
            case .breast:
                print("\(patientToCheck.name) has breast pains, should make an X-ray to be treated.")
            case .limb:
                print("\(patientToCheck.name) has limb pains, should be treated with salve.")
            case .head:
                print("\(patientToCheck.name) has headache, should be treated with Aspirin.")
            }
        }
        print()
    }

    func treatPatient(patientToTreat: Patient) {
        if patientToTreat.temperature > 40 {
            print("For \(patientToTreat.name)'s flu, he's appointed to an injection.")
        } else if patientToTreat.temperature > 37 {
            print("For \(patientToTreat.name)'s cold, he's appointed to hot tea bathes and some pills.")
        } else if patientToTreat.bodypartInPain.isEmpty {
            print("For \(patientToTreat.name)'s imagined beasts, he's appointed to bar sessions.")
        }

        patientToTreat.bodypartInPain.forEach {
            switch $0 {
            case .abdomen:
                print("For \(patientToTreat.name)'s abdomen pains, he's appointed to special medicine.")
                patientToTreat.bodypartInPain.removeAll(where: { $0 == .abdomen })
            case .breast:
                print("For \(patientToTreat.name)'s breast pains, he's appointed to an X-ray.")
                print("After X-ray \(patientToTreat.name) is treated accordingly.")
                patientToTreat.bodypartInPain.removeAll(where: { $0 == .breast })
            case .limb:
                print("For \(patientToTreat.name)'s limb pains, he's appointed to salve treatment.")
                patientToTreat.bodypartInPain.removeAll(where: { $0 == .limb })
            case .head:
                print("For \(patientToTreat.name)'s headache, he's appointed to Aspirin pill treatment.")
                patientToTreat.bodypartInPain.removeAll(where: { $0 == .head })
            }
        }
    }

    func dayendReport() {
        report.sort { $0.name < $1.name }
        report.sort { $0.bodypartsHurt.rawValue < $1.bodypartsHurt.rawValue }
        report.forEach {
            print("\($0.name) - \($0.bodypartsHurt)")
        }
    }
}
