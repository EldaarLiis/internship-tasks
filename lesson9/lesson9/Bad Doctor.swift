//
//  Bad Doctor.swift
//  lesson9
//
//  Created by Harbros 66 on 23.05.21.
//

import Foundation

class BadDoctor: PatientDelegate {
    var report: [(name: String, bodypartsHurt: BodyPart)] = []

    func checkPatient(patientToCheck: Patient) {
        if patientToCheck.temperature > 40 {
            print("\(patientToCheck.name) has cosmic illness, should be treated with magic water.")
        } else if patientToCheck.temperature > 37 {
            print("\(patientToCheck.name) has illuminati attention, should be equiped with foil hat.")
        } else {
            print("\(patientToCheck.name)'s troubles are his inner demon, trying to consume his soul.")
        }

        patientToCheck.bodypartInPain.forEach {
            switch $0 {
            case .abdomen:
                print("\(patientToCheck.name) has magnetic pollution, should get positive magnetic treatment.")
            case .breast:
                print("\(patientToCheck.name) has heart eater, should drink ionized water.")
            case .limb:
                print("\(patientToCheck.name) has hellfiend syndrome, should be treated with prayer to the god.")
            case .head:
                print("\(patientToCheck.name) has braineater bug, should be treated with magic rituals.")
            }
        }
        print()
    }

    func treatPatient(patientToTreat: Patient) {
        if patientToTreat.temperature > 40 {
            print("For \(patientToTreat.name)'s cosmic illness, he's appointed to a magic water.")
        } else if patientToTreat.temperature > 37 {
            print("For \(patientToTreat.name)'s illuminati attention, he's equiped with foil hat.")
        } else if patientToTreat.bodypartInPain.isEmpty {
            print("For \(patientToTreat.name)'s inner demon, he's appointed to devil expelling ritual.")
        }

        patientToTreat.bodypartInPain.forEach {
            switch $0 {
            case .abdomen:
                print("For \(patientToTreat.name)'s magnetic pollution, he's appointed to positive magnetic treatment.")
                if patientToTreat.bodypartInPain.contains(.head) {
                    patientToTreat.bodypartInPain.append(.head)
                }
            case .breast:
                print("For \(patientToTreat.name)'s heart eater, he's appointed with ionized water.")
                if patientToTreat.bodypartInPain.contains(.abdomen) {
                    patientToTreat.bodypartInPain.append(.abdomen)
                }
            case .limb:
                print("For \(patientToTreat.name)'s hellfiend syndrome, he's appointed to prayer to the god.")
            case .head:
                print("For \(patientToTreat.name)'s braineater bug, he's appointed to magic rituals.")
            }
        }
    }

    func dayendReport() {
        report.sort { $0.name < $1.name }
        report.sort { $0.bodypartsHurt.rawValue < $1.bodypartsHurt.rawValue }
        report.forEach {
            print("\($0.name) - \($0.bodypartsHurt)")
        }
    }
}
