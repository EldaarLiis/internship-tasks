//
//  Delegate Testing.swift
//  lesson9
//
//  Created by Harbros 66 on 21.05.21.
//

import Foundation

class Lesson9DelegateTask {
    let patientJames = Patient(patientName: "James", patientTemperature: 39, bodypartHurts: [.head, .limb])
    let patientJohn = Patient(patientName: "John", patientTemperature: 37, bodypartHurts: [.abdomen])
    let patientHannah = Patient(patientName: "Hannah", patientTemperature: 42, bodypartHurts: [])

    let doctor = GoodDoctor()
    let friend = BadDoctor()
    let anotherGoodDoctor = GoodDoctor()

    func delegateTask() {
        let hospitalWard = [patientJohn, patientJames, patientHannah]
        let doctorList: [PatientDelegate] = [doctor, friend, anotherGoodDoctor]
        hospitalWard.forEach {
            $0.doctorDelegate = doctorList.randomElement()
            $0.patentStateWorsened()
        }

        hospitalWard.forEach {
            if let patientgrade = $0.grade {
                if patientgrade > 4 {
                    $0.doctorDelegate = doctorList.randomElement()
                }
            }
        }

        doctorList.forEach {
            $0.dayendReport()
        }
    }
}
